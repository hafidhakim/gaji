<!-- ============================================================== -->
<!-- 						Topbar Start 							-->
<!-- ============================================================== -->
<div class="top-bar light-top-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <a class="admin-logo" href="index.html">
                    Aldo Accs
                </a>
                <ul class="list-inline top-right-nav">
                    <li class="dropdown avtar-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" class="rounded-circle" src="{{asset('assets/fixed-plus/img/avtar-2.png') }}" width="30">
                            {{ auth()->user()->username }}
                        </a>
                        <ul class="dropdown-menu top-dropdown">
                            <!-- <li>
                                <a class="dropdown-item" href="javascript:%20void(0);"><i class="icon-settings"></i> Pengaturan</a>
                            </li>
                            <li class="dropdown-divider"></li> -->
                            <li>
                                <a class="dropdown-item" href="{{ url('logout') }}"><i class="icon-logout"></i> Keluar Akun</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!--                        Topbar End                              -->
<!-- ============================================================== -->
