<!-- ============================================================== -->
<!-- 						Navigation Start 						-->
<!-- ============================================================== -->
<div class="main-horizontal-nav">
    <nav>
        <!-- Menu Toggle btn-->
        <div class="menu-toggle">
            <h3>Menu</h3>
            <button type="button" id="menu-btn">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <ul id="respMenu" class="ace-responsive-menu" data-menu-style="horizontal">
            <li>
                <a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a>
            </li>
            <!-- <li>
                <a href="{{ url('absensi') }}"><i class="fa fa-codepen"></i> Absensi</a>
            </li> -->
            <li>
                <a href="#"><i class="fa fa-codepen"></i> Master <span class="arrow "></span></a>
                <ul>
                    <li><a href="{{ url('master/pengguna') }}">Pengguna</a></li>
                    <li><a href="{{ url('master/karyawan') }}">Karyawan</a></li>
                    <li><a href="{{ url('master/department') }}">Department</a></li>
                    <!-- <li><a href="{{ url('master/supplier') }}">Supplier</a></li> -->
                </ul>
            </li>
            <!-- <li>
                <a href="#"><i class="fa fa-codepen"></i> Produk <span class="arrow "></span></a>
                <ul>
                    <li><a href="{{ url('master/satuan') }}">Satuan</a></li>
                    <li><a href="{{ url('master/kategori') }}">Kategori</a></li>
                    <li><a href="{{ url('master/produk') }}">Produk</a></li>
                </ul>
            </li> -->
            <li>
                <a href="#"><i class="fa fa-codepen"></i> Transaksi <span class="arrow "></span></a>
                <ul>
                    <li><a href="{{ url('transaksi/penggajian') }}">Penggajian</a></li>
                    <li><a href="{{ url('transaksi/komisi') }}">Komisi</a></li>
                    <!-- <li><a href="{{ url('transaksi/returjual') }}">Retur Jual</a></li>
                    <li><a href="{{ url('transaksi/pembelian') }}">Pembelian</a></li>
                    <li><a href="{{ url('transaksi/penerimaan') }}">Penerimaan</a></li>
                    <li><a href="{{ url('transaksi/pembayaran') }}">Pembayaran</a></li>
                    <li><a href="{{ url('transaksi/pengiriman') }}">Pengiriman</a></li> -->
                </ul>
            </li>
            <!-- <li>
                <a href="#"><i class="fa fa-codepen"></i> Laporan <span class="arrow "></span></a>
                <ul>
                    <li><a href="{{ url('laporan/labarugi') }}">Laba Rugi</a></li>
                    <li><a href="{{ url('laporan/penjualan') }}">Penjualan</a></li>
                    <li><a href="{{ url('laporan/returjual') }}">Retur Jual</a></li>
                    <li><a href="{{ url('laporan/pembelian') }}">Pembelian</a></li>
                    <li><a href="{{ url('laporan/penerimaan') }}">Penerimaan</a></li>
                    <li><a href="{{ url('laporan/pembayaran') }}">Pembayaran</a></li>
                    <li><a href="{{ url('laporan/pengiriman') }}">Pengiriman</a></li>
                </ul>
            </li> -->
        </ul>
    </nav>
</div>
<!-- ============================================================== -->
<!-- 						Navigation End	 						-->
<!-- ============================================================== -->
