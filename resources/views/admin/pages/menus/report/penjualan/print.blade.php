<html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Laporan Penjualan</title>
  <style>
  @page { margin: 20px; }
  .page-break {
    page-break-after: always;
  }
  </style>
</head><body style="padding: 0 50px;">
<br>
<br>
<br>
<br>
<br>
<div style="text-align: center; margin-bottom: 25px;">
<strong style="font-size: 24px;">LAPORAN PENJUALAN</strong>
</div>
<div style="margin-bottom: 5px;">
Tanggal Transaksi pada <strong>{{ date('d F Y', strtotime($date_start)) }} sampai {{ date('d F Y', strtotime($date_end)) }}</strong>
</div>
<table border="1" cellspacing="0" cellpadding="10" width="100%">
    <thead>
    <tr>
        <td align="center">
        <strong>Code</strong>
        </td>
        <td align="center">
        <strong>Pelanggan</strong>
        </td>
        <td align="center">
        <strong>Tanggal</strong>
        </td>
        <td align="center">
        <strong>Status Tagihan</strong>
        </td>
        <td align="center">
        <strong>Total Harga</strong>
        </td>
    </tr>
    </thead>
    <tbody>
    <?php $grand_total = 0; ?>
    @foreach($penjualan as $item)
    <tr>
        <td>{{ $item->code }}</td>
        <td align="center">{{ $item->pelanggan ? $item->pelanggan->name : '-' }}</td>
        <td align="center">{{ date('d F Y', strtotime($item->date)) }}</td>
        <td align="center">{{ $item->status_tagihan }}</td>
        <td align="right">{{ currencyFormat($item->price_total) }}</td>
    </tr>
    <?php $grand_total += $item->price_total; ?>
    @endforeach
    <tr>
        <td colspan="4" align="center">
        <strong>TOTAL</strong>
        </td>
        <td align="right">{{ currencyFormat($grand_total) }}</td>
    </tr>
    </tbody>
</table>
<div style="text-align: right;margin-top: 20px;">
Salam,
<br>
Admin Yabes Optical
</div>
</body></html>
