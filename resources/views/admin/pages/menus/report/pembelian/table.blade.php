@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Laporan Pembelian</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Laporan Pembelian</li>		
    </ol>
  </div>
  <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('laporan/pembelian/print?date_range='.$date_range) }}" class="btn btn-warning box-shadow btn-icon btn-rounded"><i class="fa fa-print"></i> Cetak Laporan</a>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Laporan Pembelian
        <div class="pull-right">
          <form method="get" action="">
            <label for="date_range" class="pull-right">Filter Tanggal</label>
            <div class="input-group">
              <input type="text" name="date_range" id="date_range" class="form-control pull-right datepicker" style="min-width: 100px;" value="">
              <div class="input-group-append">
                <button type="submit" class="btn btn-primary">Filter</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="card-body">
        <table id="datatable" class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Code</th>
                    <th>Supplier</th>
                    <th>Tanggal</th>
                    <th>Total Harga</th>
                    <th>Status Tagihan</th>
                    <th width="100">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->code }}</td>
                        <td>{{ $item->supplier ? $item->supplier->name : '-' }}</td>
                        <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                        <td>{{ currencyFormat($item->price_total) }}</td>
                        <td>{{ $item->status_tagihan }}</td>
                        <td>
                            <a href="{{ url('transaksi/pembelian/'.$item->id) }}" class="btn btn-primary">Detail</a>
                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "order": [[ 0, "desc" ]]
    });

    $('.datepicker').daterangepicker({
      locale: {
        format: 'YYYY/MM/DD'
      }
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection