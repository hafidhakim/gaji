@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Absensi</h2>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header card-default">
            Buat Data Absensi
        </div>
        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form method="post" class="form-horizontal" action="">
            {{ csrf_field() }}
            @if (auth()->user()->role == 'admin')
            <div class="form-group">
              <label for="pengguna_id">Nama Pegawai</label>
              <select name="pengguna_id" id="pengguna_id" class="form-control select2">
                <option value="">Pilih Pegawai</option>
                @foreach ($pegawai as $item)
                  <option value="{{ $item->id }}">{{ $item->username }}</option>
                @endforeach
              </select>
            </div>
            @else
            <div class="form-group">
              <label for="pengguna_id">Nama Pegawai</label>
              <select name="pengguna_id" id="pengguna_id" class="form-control" readonly>
                <option value="">Pilih Pegawai</option>
                @foreach ($pegawai as $item)
                  @if (auth()->user()->id == $item->id)
                  <option value="{{ $item->id }}" selected>{{ $item->username }}</option>
                  @else
                  <option value="{{ $item->id }}">{{ $item->username }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            @endif
            <div class="form-group">
              <label for="date">Tanggal</label>
              <input type="text" name="date" id="date" class="datepicker" value="{{ date('Y-m-d') }}" required/>
            </div>
            <div class="form-group">
              <label for="time_start">Jam Mulai Kerja</label>
              <input type="text" name="time_start" id="time_start" class="timepicker" value="{{ date('H:m') }}" required/>
            </div>
            <div class="form-group">
              <label for="time_end">Jam Selesai Kerja</label>
              <input type="text" name="time_end" id="time_end" class="timepicker" value="{{ date('H:m', strtotime('+8 hours')) }}" required/>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection


@section('js')
<script>
$(document).ready(function(){
  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });
  $('.timepicker').clockpicker({
    placement: 'top',
    align: 'left',
    donetext: 'Selesai'
  });
});
</script>
@endsection