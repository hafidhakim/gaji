@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Master Supplier</h2>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header card-default">
            Buat Data Baru
        </div>
        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form method="post" class="form-horizontal" action="">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="code">Kode</label>
              <input type="text" value="{{ old('code') ? old('code') : $newcode }}" class="form-control" id="code" name="code" readonly/>
            </div>
            <div class="form-group">
              <label for="name">Nama Supplier</label>
              <input type="text" value="{{ old('name') }}" class="form-control" id="name" name="name" />
            </div>
            <div class="form-group">
              <label for="phone">Telepon</label>
              <input type="number" value="{{ old('phone') }}" class="form-control md-input masked_input" id="phone" name="phone" data-inputmask="'mask': '9', 'repeat': 14, 'greedy' : false"/>
            </div>
            <div class="form-group">
              <label for="address">Alamat</label>
              <input type="text" value="{{ old('address') }}" class="form-control" id="address" name="address" />
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('more-js')
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    // thisform.m_init();
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
};
</script>
@endsection
