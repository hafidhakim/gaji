@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Master Karyawan</h2>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header card-default">
            Buat Data Baru
        </div>
        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form method="post" class="form-horizontal" action="">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="code">Kode</label>
              <input type="text" value="{{ old('code') ? old('code') : $newcode }}" class="form-control" id="code" name="code" readonly />
            </div>
            <div class="form-group">
              <label for="name">Nama Karyawan</label>
              <input type="text" value="{{ old('name') }}" class="form-control" id="name" name="name" />
            </div>
            <div class="form-group">
              <label for="department_id">Department</label>
              <select name="department_id" id="department_id" class="form-control select2">
                @foreach($department as $item)
                  @if(old('department_id') == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                  @else
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="gaji_pokok">Gaji Pokok</label>
              <input type="text" value="{{ old('gaji_pokok',0) }}" class="form-control" id="gaji_pokok" name="gaji_pokok" />
            </div>
            <div class="form-group">
              <label for="tunjangan_jabatan">Tunjangan Jabatan</label>
              <input type="text" value="{{ old('tunjangan_jabatan',0) }}" class="form-control" id="tunjangan_jabatan" name="tunjangan_jabatan" />
            </div>
            <div class="form-group">
              <label for="phone">Telepon</label>
              <input type="text" value="{{ old('phone') }}" class="form-control" id="phone" name="phone" />
            </div>
            <div class="form-group">
              <label for="address">Alamat</label>
              <input type="text" value="{{ old('address') }}" class="form-control" id="address" name="address" />
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
