@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Master Kategori</h2>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header card-default">
            Buat Data Baru
        </div>
        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form method="post" class="form-horizontal" action="">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="name">Nama Kategori</label>
              <input type="text" value="{{ old('name') }}" class="form-control" id="name" name="name" placeholder="e.g : Frame, Lensa" />
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection