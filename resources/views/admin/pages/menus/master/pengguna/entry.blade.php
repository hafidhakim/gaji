@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Master Pengguna</h2>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header card-default">
            Buat Data Baru
        </div>
        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <form method="post" class="form-horizontal" action="">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" value="{{ old('username') }}" class="form-control" id="username" name="username" />
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" />
            </div>
            <div class="form-group">
              <label for="role">Role</label>
              <select name="role" id="role" class="form-control">
                @if(old('role') == "admin")
                  <option value="admin" selected>admin</option>
                @else
                  <option value="admin">admin</option>
                @endif
                @if(old('role') == "pegawai")
                  <option value="pegawai" selected>pegawai</option>
                @else
                  <option value="pegawai">pegawai</option>
                @endif
              </select>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function(){
  $('body').on('change', '.all', function(){
    if ($(this).prop('checked') == true) {
      $($(this).data('target') + ' input[type="checkbox"]').prop('checked', true);
    }else {
      $($(this).data('target') + ' input[type="checkbox"]').prop('checked', false);
    }
  });

  $('body').on('change', '.all-sub', function(){
    if ($(this).prop('checked') == true) {
      $($(this).data('target')).prop('checked', true);
    }else {
      $($(this).data('target')).prop('checked', false);
    }
  });
});
</script>
@endsection
