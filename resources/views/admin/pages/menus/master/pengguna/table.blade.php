@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Pengguna</h2>
  </div>
  <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('master/pengguna/create') }}" class="btn btn-success box-shadow btn-icon btn-rounded"><i class="fa fa-plus"></i> Buat Baru</a>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Master Pengguna
      </div>
      <div class="card-body">
        <table id="datatable" class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th width="200">&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->username }}</td>
                        <td>{{ $item->role }}</td>
                        <td>
                            <a href="{{ url('master/pengguna/' . $item->eid) }}" class="btn btn-warning">Ubah</a>
                            <a href="{{ url('master/pengguna/delete/' . $item->eid) }}" onclick="return confirm('Apa anda yakin ingin menghapus data ini?')" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "order": [[ 0, "desc" ]]
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection