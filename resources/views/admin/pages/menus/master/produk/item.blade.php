<tr>
    <td>
        <div class="input-group">
        <select name="produk_id[]" class="form-control" required readonly>
            <option value="">Pilih Produk</option>
            @foreach($data as $item)
            <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
            @endforeach
        </select>
        <div class="input-group-append">
            <button class="btn btn-outline-secondary open-produk-modal" type="button" data-target="{{ $countrow }}">Cari</button>
        </div>
        </div>
    </td>
    <td>-</td>
    @if ($price_custom == 'true')
    <td>
        <input type="number" class="form-control" name="produk_price[]" value="0" min="0" />
    </td>
    @else
    <td>-</td>
    @endif
    <td>-</td>
    <td>-</td>
    <td>
        <input type="number" class="form-control" name="qty[]" value="1" min="1" />
    </td>
    <td>-</td>
    <td>
        <a href="#" class="btn btn-danger remove-item">Hapus</a>
    </td>
</tr>