@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Pembelian Baru</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/pembelian') }}">Transaksi Pembelian</a></li>
      <li class="breadcrumb-item active">Baru</li>
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">

        <div class="card-header card-default">
            Buat Data Baru
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}

            <div class="row">

              <div class="col-md-2">
                <h5>Tanggal Pemesanan</h5>
                <div class="form-group">
                  <div class="input-group m-b">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                    <input type="text" name="date" class="datepicker" required />
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <h5>Supplier</h5>
                <div class="input-group">
                  <select name="suplier_id" id="suplier_id" class="form-control" required readonly>
                    <option value="">Pilih Supplier</option>
                    @foreach($supplier as $item)
                      <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
                    @endforeach
                  </select>
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#supplier-modal">Cari</button>
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <h5>Pembayaran</h5>
                <div class="input-group">
                  <select name="pembayaran" id="pembayaran" class="form-control" required>
                    <option value="Cash">Cash</option>
                    <option value="Angsur">Angsur</option>
                  </select>
                </div>
              </div>

              <div class="col-md-3">
                <h5>Nominal Pembayaran</h5>
                <div class="form-group">
                    <input type="number" name="amount_pay" id="amount_pay" value="0" min="0" max="{{ $old_price_total }}" class="form-control" required readonly />
                </div>
              </div>

            </div>

            <br>
            <h5>Item Produk Pembelian</h5>
            <table class="table table-striped dt-responsive wrap table-detail">
                <thead>
                  <tr>
                    <th width="350">Nama Produk</th>
                    <th>Kategori</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Satuan</th>
                    <th width="100">Qty</th>
                    <th>Subtotal</th>
                    <th width="100">Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $countrow = 0;?>
                  @foreach($old_pembelian_detail as $itemDetail)
                    <tr>
                      <td>
                        <div class="input-group">
                        <select name="produk_id[]" class="form-control" required readonly>
                          <option value="">Pilih Produk</option>
                          @foreach($produk as $item)
                            @if ($item->id == $itemDetail['produk_id'])
                              <option value="{{ $item->id }}" selected>{{ $item->name }} ({{ $item->code }})</option>
                            @else
                              <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
                            @endif
                          @endforeach
                        </select>
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary open-produk-modal" type="button" data-target="{{ $countrow }}">Cari</button>
                        </div>
                        </div>
                      </td>
                      <td>{{ $itemDetail['kategori'] }}</td>
                      <td>
                        <input type="number" class="form-control" name="produk_price[]" value="{{ $itemDetail['produk_price'] }}" min="1" />
                      </td>
                      <td>{{ $itemDetail['stok'] }}</td>
                      <td>{{ $itemDetail['satuan'] }}</td>
                      <td>
                        <input type="number" class="form-control" name="qty[]" value="{{ $itemDetail['qty'] }}" min="1" />
                      </td>
                      <td>{{ currencyFormat($itemDetail['price_subtotal']) }}</td>
                      <td>
                        <a href="#" class="btn btn-danger remove-item">Hapus</a>
                      </td>
                    </tr>
                    <?php $countrow++;?>
                  @endforeach
                </tbody>
            </table>
            <a href="#" class="btn btn-success add-item">Tambah Item</a>
            <div class="pull-right">
              <h3>Total Dibayar: <span id="amount_pay_total">{{ currencyFormat(0) }}</span></h3>
              <h3 style="display: none;">Sisa Tagihan: <span id="amount_left_total">{{ currencyFormat(0) }}</span></h3>
              <h3>Total Tagihan: <span id="price_total">{{ currencyFormat($old_price_total) }}</span></h3>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Supplier Modal -->
<div class="modal fade" id="supplier-modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title">Daftar Supplier</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped dt-responsive wrap datatable">
              <thead>
                  <tr>
                      <th width="50">ID</th>
                      <th>Kode</th>
                      <th>Nama Supplier</th>
                      <th>Telepon</th>
                      <th>Alamat</th>
                      <th width="100">Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach($supplier as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->code }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->phone }}</td>
                          <td>{{ $item->address }}</td>
                          <td>
                              <a href="#" class="btn btn-primary select-supplier" data-target="{{ $item->id }}">Pilih</a>
                          </td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
      </div>
  </div>
</div>

<!-- Produk Modal -->
<div class="modal fade" id="produk-modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title">Daftar Produk</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
          <table class="table table-striped dt-responsive wrap datatable">
              <thead>
                  <tr>
                      <th width="50">ID</th>
                      <th>Kode</th>
                      <th>Nama Produk</th>
                      <th>Kategori</th>
                      <th>Harga Beli</th>
                      <th>Stok</th>
                      <th>Satuan</th>
                      <th width="100">Action</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($produk as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->code }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->kategori ? $item->kategori->name : "-" }}</td>
                          <td>{{ $item->price_buy }}</td>
                          <td>{{ $item->stock }}</td>
                          <td>{{ $item->satuan ? $item->satuan->name : "-" }}</td>
                          <td>
                              <a href="#" class="btn btn-primary select-produk" data-target="{{ $item->id }}">Pilih</a>
                          </td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
      </div>
  </div>
</div>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('click', '.select-supplier', function(){
    var id = $(this).data('target');
    $('#suplier_id').val(id);
    $('#supplier-modal').modal('hide');

    return false;
  });

  $('body').on('change', '#pembayaran', function(){
    if ($(this).val() == "Angsur") {
      $("#amount_pay").val(0);
      $("#amount_pay_total").html(numberWithCurrency(0));
      $("#amount_left_total").parents('h3').show();
      $("#amount_pay").removeAttr('readonly');
    }else {
      $("#amount_left_total").parents('h3').hide();
      $("#amount_pay").attr('readonly', true);
      loadPriceTotal();
    }
  });

  $('body').on('change', '#amount_pay', function(){
    var amountPay = $(this).val();
    var amountPayMax = parseInt($(this).attr('max'));
    var amountLeft = 0
    var priceTotal = $('#price_total').html().replace("Rp ", "").replace(",-", "").replace(/\./g, "");

    if (amountPay > amountPayMax) {
      $(this).val(amountPayMax);
    }else {
      amountLeft = parseInt(priceTotal) - parseInt(amountPay);
      $("#amount_pay_total").html(numberWithCurrency(amountPay));
    }

    $('#amount_left_total').html(numberWithCurrency(amountLeft));
  });

  function addItemDetail() {
    var countRow = $('.table-detail tbody tr').length;

    $.ajax({
      url: "{{ url('ajax/produk/item-html') }}",
      method: "GET",
      data: "countrow=" + countRow + "&price_custom=true",
      success: function(response) {
        $('.table-detail tbody').append(response);
      }
    });
  }

  var countRow = $('.table-detail tbody tr').length;
  if (countRow <= 0) {
    addItemDetail();
  }
  $('body').on('click', '.add-item', function(){
    addItemDetail();

    return false;
  });
  $('body').on('click', '.remove-item', function(){
    $(this).parents('tr').remove();

    return false;
  });

  var produkTargetRow = "";

  $('body').on('click', '.open-produk-modal', function () {
    produkTargetRow = $(this).data('target');
    $('#produk-modal').modal('show');
  });

  function loadPriceTotal() {
    var price_total = 0;
    $( ".table-detail tbody tr" ).each(function( index ) {
      price_total += parseInt($(this).children('td').eq(6).html().replace("Rp ", "").replace(",-", "").replace(/\./g, ""));
    });

    if (price_total > 0) {
      $('#price_total').html(numberWithCurrency(price_total));
      $('#amount_pay').attr('max', price_total);
      if ($('#pembayaran').val() == "Cash") {
        $('#amount_pay').val(price_total);
        $("#amount_pay_total").html(numberWithCurrency(price_total));
      }
    }
  }

  $('body').on('click', '.select-produk', function(){
    var id = $(this).data('target');
    var kategori = $(this).parents('tr').children('td').eq(3).html();
    var harga = $(this).parents('tr').children('td').eq(4).html();
    var stok = $(this).parents('tr').children('td').eq(5).html();
    var satuan = $(this).parents('tr').children('td').eq(6).html();
    var qty = $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").val();
    $('.table-detail tbody tr').eq(produkTargetRow).find("select[name='produk_id[]']").val(id);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(1).html(kategori);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(2).find("input").val(harga);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(2).find("input").attr("min", harga);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(3).html(stok);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(4).html(satuan);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").focus();
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(6).html(numberWithCurrency(qty * harga));
    $('#produk-modal').modal('hide');
    loadPriceTotal();

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="produk_price[]"]', function(){
    var harga = parseInt($(this).val());
    var hargaMinimum = parseInt($(this).attr('min'));
    var qty = $(this).parents('tr').children('td').eq(5).find('input').val();

    if (harga < hargaMinimum) {
      $(this).val(hargaMinimum);
    }else {
      $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
      loadPriceTotal();
    }

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="qty[]"]', function(){
    var harga = $(this).parents('tr').children('td').eq(2).find('input').val();
    var qty = $(this).val();
    $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
    loadPriceTotal();

    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection
