<html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Bukti Penjualan</title>
  <style>
  @page { margin: 20px; }
  .page-break {
    page-break-after: always;
  }
  </style>
</head><body style="padding: 0 50px;">
<br>
<br>
<br>
<br>
<br>
<div style="text-align: center; margin-bottom: 25px;">
<strong style="font-size: 24px;">BUKTI PENJUALAN</strong>
</div>
<div style="margin-bottom: 5px;">
Untuk pelanggan <strong>{{ $penjualan->pelanggan->name }}</strong>
</div>
<div style="margin-bottom: 5px;">
Alamat: <strong>{{ $penjualan->pelanggan->address or "-" }}</strong>
</div>
<div style="margin-bottom: 5px;">
Telp: <strong>{{ $penjualan->pelanggan->phone or "-" }}</strong>
</div>
<div style="margin-bottom: 10px;">
Berikut pesanan barang yang ingin anda beli
</div>
<table border="1" cellspacing="0" cellpadding="10" width="100%">
    <thead>
    <tr>
        <td align="center">
        <strong>Nama Produk</strong>
        </td>
        <td align="center">
        <strong>Kategori</strong>
        </td>
        <td align="center">
        <strong>Harga</strong>
        </td>
        <td align="center">
        <strong>Satuan</strong>
        </td>
        <td align="center">
        <strong>Qty</strong>
        </td>
        <td align="center">
        <strong>Subtotal</strong>
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($penjualan->detail as $item)
    <tr>
        <td>{{ $item->produk_name }}</td>
        <td align="center">{{ $item->produk->kategori ? $item->produk->kategori->name : '-' }}</td>
        <td align="right">{{ currencyFormat($item->produk_price) }}</td>
        <td align="center">{{ $item->produk->satuan ? $item->produk->satuan->name : '-' }}</td>
        <td align="center">{{ $item->qty }}</td>
        <td align="right">{{ currencyFormat($item->price_subtotal) }}</td>
    </tr>
    @endforeach
    <tr>
        <td colspan="5" align="center">
        <strong>TOTAL</strong>
        </td>
        <td align="right">{{ currencyFormat($penjualan->price_total) }}</td>
    </tr>
    </tbody>
</table>
<div style="margin-top: 10px;">
Terima kasih telah membeli barang ditoko kami.
</div>
<div style="text-align: right;margin-top: 20px;">
Hormat Kami,
<br>
Admin Aldo Acc
</div>
</body></html>
