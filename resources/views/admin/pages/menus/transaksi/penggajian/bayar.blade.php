@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Pembayaran Tagihan</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/penjualan') }}">Transaksi Penjualan</a></li>
      <li class="breadcrumb-item"><a href="{{ url('transaksi/penjualan/'.$penjualan->id) }}">{{ $penjualan->code }}</a></li>
      <li class="breadcrumb-item active">Bayar Tagihan</li>		
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">

        <div class="card-header card-default">
            Bayar Tagihan
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}
              
            <div class="row">
                
                <div class="col-md-3">
                    <h5>Tanggal Pembayaran</h5>
                    <div class="form-group">
                        <div class="input-group m-b">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" name="date" class="datepicker" value="{{ date('Y-m-d') }}" required />
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <h5>Ingin Bayar</h5>
                    <div class="form-group">
                        <input type="number" class="form-control" name="amount_pay" id="amount_pay" value="0" min="0" max="{{ $penjualan->sisa_bayar }}" required />
                    </div>
                </div>
            </div>

            <div class="pull-right">
              <h3>Total Dibayar: <span id="amount_pay_total">{{ currencyFormat(0) }}</span></h3>
              <h3>Sisa Tagihan: <span id="amount_left_total">{{ currencyFormat($penjualan->sisa_bayar) }}</span></h3>
              <h3>Total Tagihan: <span id="price_total">{{ currencyFormat($penjualan->price_total) }}</span></h3>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">Bayar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('change', '#amount_pay', function(){
    var amountPay = $(this).val();
    var amountPayMax = parseInt($(this).attr('max'));
    var amountLeft = 0

    if (amountPay > amountPayMax) {
      $(this).val(amountPayMax);
      $("#amount_pay_total").html(numberWithCurrency(amountPayMax));
    }else {
      amountLeft = parseInt(amountPayMax) - parseInt(amountPay);
      $("#amount_pay_total").html(numberWithCurrency(amountPay));
    }

    $('#amount_left_total').html(numberWithCurrency(amountLeft));
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection