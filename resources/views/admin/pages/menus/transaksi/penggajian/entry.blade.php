@extends('admin.master')

@section('content')

<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Penggajian Baru</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('transaksi/penggajian') }}">Transaksi Penggajian</a></li>
      <li class="breadcrumb-item active">Baru</li>
    </ol>
  </div>
</div>

<section class="main-content">
  <div class="row">
    <div class="col-sm-6">
      <div class="card">

        <div class="card-header card-default">
            Buat Data Baru
        </div>

        <div class="card-body">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form method="post" class="form-horizontal" id="form-utama" action="">
            {{ csrf_field() }}

            <div class="row">

              <div class="col-md-4">
                <h5>Tanggal</h5>
                <div class="form-group">
                  <div class="input-group m-b">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                    <input type="text" name="date" class="datepicker" required />
                  </div>
                </div>
              </div>

              <div class="col-md-8">
                <h5>Karyawan</h5>
                <div class="input-group">
                  <select name="pegawai_id" id="pegawai_id" class="form-control" required readonly>
                    <option value="">-</option>
                    @foreach($pelanggan as $item)
                      <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->code }})</option>
                    @endforeach
                  </select>
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#pelanggan-modal">Cari</button>
                  </div>
                </div>
              </div>
            </div>

            <br>
            <h5><b>Gaji</h5>
            <div class="form-group">
              <label for="gaji_pokok">Gaji Pokok</label>
              <input type="number" value="{{ old('gaji_pokok') }}" class="form-control" id="gaji_pokok" name="gaji_pokok" readonly />
              <!-- <a class="btn btn-warning btn-ubah" target="blank_">Ubah</a> -->
            </div>
            <div class="form-group">
              <label for="tunjangan_jabatan">Tunjangan Jabatan</label>
              <input type="number" value="{{ old('tunjangan_jabatan',0) }}" class="form-control" id="tunjangan_jabatan" name="tunjangan_jabatan" />
            </div>
            <div class="form-group">
              <label for="uang_makan">Uang Makan</label>
              <input type="number" value="{{ old('uang_makan',0) }}" class="form-control" id="uang_makan" name="uang_makan" />
            </div>
            <div class="form-group">
              <label for="uang_premi">Uang Premi</label>
              <input type="number" value="{{ old('uang_premi',200000) }}" class="form-control" id="uang_premi" name="uang_premi" />
            </div>
            <div class="form-group">
              <label for="gaji_lain">Gaji Lain-lain</label>
              <input type="number" value="{{ old('gaji_lain',0) }}" class="form-control" id="gaji_lain" name="gaji_lain" />
            </div>
            <br>
            <h5><b>Potongan</h5>
            <div class="form-group">
              <label for="bon_barang">Bon Barang</label>
              <input type="number" value="{{ old('bon_barang',0) }}" class="form-control" id="bon_barang" name="bon_barang" />
            </div>
            <div class="form-group">
              <label for="bon_uang">Bon Uang</label>
              <input type="number" value="{{ old('bon_uang',0) }}" class="form-control" id="bon_uang" name="bon_uang" />
            </div>
            <div class="form-group">
              <label for="terlambat">Terlambat</label>
              <input type="number" value="{{ old('terlambat',0) }}" class="form-control" id="terlambat" name="terlambat" />
            </div>
            <div class="form-group">
              <label for="potongan_lain">Potongan Lain-lain</label>
              <input type="number" value="{{ old('potongan_lain',0) }}" class="form-control" id="potongan_lain" name="potongan_lain" />
            </div>
            <br>
            <br>
            <br>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Pelanggan Modal -->
<div class="modal fade" id="pelanggan-modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h3 class="modal-title">Daftar Karyawan</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped dt-responsive wrap datatable">
              <thead>
                  <tr>
                      <th width="50">ID</th>
                      <th>Kode</th>
                      <th>Nama Karyawan</th>
                      <th>Gaji Pokok</th>
                      <th>Tunj. Jabatan</th>
                      <th>Alamat</th>
                      <th width="100">Action</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach($pelanggan as $item)
                      <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->code }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->gaji_pokok }}</td>
                          <td>{{ $item->tunjangan_jabatan }}</td>
                          <td>{{ $item->address }}</td>
                          <td>
                              <a href="#" class="btn btn-primary select-pelanggan" data-target="{{ $item->id }}">Pilih</a>
                          </td>
                      </tr>
                  @endforeach
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          </div>
      </div>
  </div>
</div>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('click', '.select-pelanggan', function(){
    var id = $(this).data('target');
    var gapok = $(this).parents('tr').children('td').eq(3).html();
    var tunj = $(this).parents('tr').children('td').eq(4).html();
    $("#gaji_pokok").val(gapok);
    $("#tunjangan_jabatan").val(tunj);
    $('#pegawai_id').val(id);
    $('.btn-ubah').attr('href',"{{ url('master/pelanggan') }}"+'/'+id);
    $('#pelanggan-modal').modal('hide');

    return false;
  });

  $('body').on('change', '#pembayaran', function(){
    if ($(this).val() == "Angsur") {
      $("#amount_pay").val(0);
      $("#amount_pay_total").html(numberWithCurrency(0));
      $("#amount_left_total").parents('h3').show();
      $("#amount_pay").removeAttr('readonly');
    }else {
      $("#amount_left_total").parents('h3').hide();
      $("#amount_pay").attr('readonly', true);
      loadPriceTotal();
    }
  });

  $('body').on('change', '#amount_pay', function(){
    var amountPay = $(this).val();
    var amountPayMax = parseInt($(this).attr('max'));
    var amountLeft = 0
    var priceTotal = $('#price_total').html().replace("Rp ", "").replace(",-", "").replace(/\./g, "");

    if (amountPay > amountPayMax) {
      $(this).val(amountPayMax);
    }else {
      amountLeft = parseInt(priceTotal) - parseInt(amountPay);
      $("#amount_pay_total").html(numberWithCurrency(amountPay));
    }

    $('#amount_left_total').html(numberWithCurrency(amountLeft));
  });

  function addItemDetail() {
    var countRow = $('.table-detail tbody tr').length;

    $.ajax({
      url: "{{ url('ajax/produk/item-html') }}",
      method: "GET",
      data: "countrow=" + countRow + "&price_custom=false",
      success: function(response) {
        $('.table-detail tbody').append(response);
      }
    });
  }

  var countRow = $('.table-detail tbody tr').length;
  if (countRow <= 0) {
    addItemDetail();
  }
  $('body').on('click', '.add-item', function(){
    addItemDetail();

    return false;
  });
  $('body').on('click', '.remove-item', function(){
    $(this).parents('tr').remove();

    return false;
  });

  var produkTargetRow = "";

  $('body').on('click', '.open-produk-modal', function () {
    produkTargetRow = $(this).data('target');
    $('#produk-modal').modal('show');
  });

  function loadPriceTotal() {
    var price_total = 0;
    $( ".table-detail tbody tr" ).each(function( index ) {
      price_total += parseInt($(this).children('td').eq(6).html().replace("Rp ", "").replace(",-", "").replace(/\./g, ""));
    });

    if (price_total > 0) {
      $('#price_total').html(numberWithCurrency(price_total));
      $('#amount_pay').attr('max', price_total);
      if ($('#pembayaran').val() == "Cash") {
        $('#amount_pay').val(price_total);
        $("#amount_pay_total").html(numberWithCurrency(price_total));
      }
    }
  }

  $('body').on('click', '.select-produk', function(){
    var id = $(this).data('target');
    var kategori = $(this).parents('tr').children('td').eq(3).html();
    var harga = $(this).parents('tr').children('td').eq(4).html();
    var stok = $(this).parents('tr').children('td').eq(5).html();
    var satuan = $(this).parents('tr').children('td').eq(6).html();
    var qty = $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").val();
    $('.table-detail tbody tr').eq(produkTargetRow).find("select[name='produk_id[]']").val(id);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(1).html(kategori);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(2).html(numberWithCurrency(harga));
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(3).html(stok);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(4).html(satuan);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").focus();
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(6).html(numberWithCurrency(qty * harga));
    $('#produk-modal').modal('hide');
    loadPriceTotal();

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="produk_price[]"]', function(){
    var harga = parseInt($(this).val());
    var hargaMinimum = parseInt($(this).attr('min'));
    var qty = $(this).parents('tr').children('td').eq(5).find('input').val();

    if (harga < hargaMinimum) {
      $(this).val(hargaMinimum);
    }else {
      $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
      loadPriceTotal();
    }

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="qty[]"]', function(){
    var harga = $(this).parents('tr').children('td').eq(2).html().replace("Rp ", "").replace(",-", "").replace(/\./g, "");
    var qty = $(this).val();
    $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
    loadPriceTotal();

    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection
