<?php
error_reporting(E_ALL ^ E_DEPRECATED);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Slip Gaji </title>
    <style>
        @page {
            margin: 10px;
        }

        .page-break {
            page-break-after: always;
        }

        td {
            font-size: 14px;
        }

        img {
            border-radius: 10px;
            width: 50px;
            height: auto;
            opacity: 0.8;
        }

        .center {
            margin: auto;
            text-align: center;
            margin-top: 10px;
        }

        .total {
            margin: 0;
            text-align: right;
            font-weight: 900;
        }
        #background{
            position:absolute;
            z-index:0;
            margin-top: 180px;
            letter-spacing: 8px;
            background:white;
            display:block;
            min-height:50%;
            min-width:50%;
            color:yellow;
        }
        #content{
            position:absolute;
            z-index:1;
        }
        #bg-text
        {
            color:lightgrey;
            opacity: 0.3;
            font-size:120px;
            transform:rotate(330deg);
            -webkit-transform:rotate(330deg);
        }
    </style>
</head>
<body style="padding: 0 50px;">
<div id="background">
    <p id="bg-text">Confidential</p>
</div>
<div id="content">
<div style="width: 100%; display: table; margin-top: 25px;">
    <div style="display: table-row;">
        <div style="width: 10%; display: table-cell;">
            <a href="{{url('')}}"><img src="{{asset('assets/fixed-plus/img/logos.png')}}" alt=""/></a>
        </div>
        <div style="display: table-cell;">
            <strong style="font-size: 20px;">SLIP GAJI</strong><br>
            <strong style="font-size: 20px;">ALDO ACCESSORIES</strong>
        </div>
        <div style="display: table-cell; font-size: 14px">
            Periode : {{ date('F Y', strtotime($payroll->date)) }}<br>
            <strong>Karyawan : {{ $payroll->pelanggan->name }}</strong><br>
            Divisi : {{ strtoupper($payroll->pelanggan->department->name) }}
        </div>
    </div>
</div>
<div style="margin-bottom: 5px;">
    <strong style="margin:0px; padding: 0px; font-size: 12px">DISTRIBUTOR ACCESSORIES HANDPHONE</strong>
    <p style="margin:0px; padding: 0px; font-size: 12px">Jl. Kidemang Singomenggolo No. 99, Ds. Sidomulyo</p>
    <p style="margin:0px; padding: 0px; font-size: 12px">Kec. Buduran, Kab. Sidoarjo</p>
    <p style="margin:0px; padding: 0px; font-size: 12px">Jawa Timur 61252</p>
    <p style="margin:0px; padding: 0px; font-size: 12px">Telp : (031) 7872810</p>
</div>
<hr style="margin: 0em;border-width: 2px; margin-bottom: 5px;">
{{--<br>--}}
<table border="0" cellspacing="0" cellpadding="2" width="100%">
    <tbody>
    <tr>
        <td align="center" colspan="2">
            <span style="font-weight:bold; text-decoration: underline;">Gaji</span>
        </td>
        <td align="center" colspan="2">
            <span style="font-weight:bold; text-decoration: underline;">Potongan</span>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td><strong>Gaji Pokok </strong></td>
        <td> : {{ currencyFormat($payroll->gaji_pokok) }}</td>
        <td><strong>Bon Barang </strong></td>
        <td> : {{ currencyFormat($payroll->bon_barang) }}</td>
    </tr>
    <tr>
        <td><strong>Tunjangan Jabatan </strong></td>
        <td> : {{ currencyFormat($payroll->tunjangan_jabatan) }}  </td>
        <td><strong>Bon Uang </strong></td>
        <td> : {{ currencyFormat($payroll->bon_uang) }}</td>
    </tr>
    <tr>
        <td><strong>Uang Makan </strong></td>
        <td> : {{ currencyFormat($payroll->uang_makan) }}</td>
        <td><strong>Terlambat </strong></td>
        <td> : {{ currencyFormat($payroll->terlambat) }}</td>
    </tr>
    <tr>
        <td><strong>Uang Premi </strong></td>
        <td> : {{ currencyFormat($payroll->uang_premi) }}</td>
        <td><strong>Potongan Lain-lain </strong></td>
        <td> : {{ currencyFormat($payroll->potongan_lain) }}</td>
    </tr>
    <tr>
        <td><strong>Gaji Lain-lain </strong></td>
        <td> : {{ currencyFormat($payroll->gaji_lain) }}</td>
    </tr>
    </tbody>
</table>
<br>
<hr style="margin: 0em;border-width: 2px;">
<div class="total">
    Total : {{ currencyFormat($payroll->total) }}
</div>
<hr style="margin: 0em;border-width: 2px;">
<div class="center"><?php echo "Sidoarjo, " . date("d F Y");?></div>
<br>
<div style="text-align: left;">
    Hormat Kami,
    <br>
    <br>
    <br>
    <br>
    HRD
</div>
</div>
</body>
</html>
