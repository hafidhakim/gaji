@extends('admin.master')

@section('content')
    <div class="row page-header">
        <div class="col-lg-6 align-self-center ">
            <h2>Penggajian</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Transaksi Penggajian</li>
            </ol>
        </div>
        <div class="col-lg-6 align-self-center text-right">
            <a href="{{ url('transaksi/penggajian/create') }}"
               class="btn btn-success box-shadow btn-icon btn-rounded"><i class="fa fa-plus"></i> Buat Baru</a>
        </div>
    </div>

    <section class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-default">
                        Transaksi Penggajian
                        <div class="pull-right">
                            <form method="get" action="">
                                <label for="date_range" class="pull-right">Filter & Print</label>
                                <div class="input-group">
                                    <input type="text" name="date_range" id="date_range"
                                           class="form-control pull-right datepicker" style="min-width: 100px;"
                                           value="">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary">Filter</button>
                                        <a href="{{ url('transaksi/penggajian/prints?date_range='.$date_range) }}"
                                           target="blank_" class="btn btn-warning box-shadow btn-icon btn-rounded"><i
                                                    class="fa fa-print"></i> Print Filtered</a>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="datatable" class="table table-striped dt-responsive wrap vtable">
                            <thead>
                            <tr>
                                <th class="th1" rowspan="2" width="50">No</th>
                                <th class="th1" rowspan="2">Karyawan</th>
                                <th class="th1" rowspan="2">Tanggal</th>
                                <th class="th1" colspan="5">Gaji</th>
                                <th class="th1" colspan="4">Pengeluaran</th>
                                <th class="th1" rowspan="2">Total</th>
                                <th class="th1" rowspan="2" width="100">Action</th>
                            </tr>
                            <tr>
                                <th class="th1">Gapok</th>
                                <th class="th1">Tunj. Jabatan</th>
                                <th class="th1">Uang Makan</th>
                                <th class="th1">Premi</th>
                                <th class="th1">Gaji Lain</th>
                                <th class="th1">Bon Barang</th>
                                <th class="th1">Bon Uang</th>
                                <th class="th1">Terlambat</th>
                                <th class="th1">Pot Lain</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->pelanggan->name}}</td>
                                    <td>{{ date('d F Y', strtotime($item->date)) }}</td>
                                    <td>{{ $item->gaji_pokok }}</td>
                                    <td>{{ $item->tunjangan_jabatan }}</td>
                                    <td>{{ $item->uang_makan }}</td>
                                    <td>{{ $item->uang_premi }}</td>
                                    <td>{{ $item->gaji_lain }}</td>
                                    <td>{{ $item->bon_barang }}</td>
                                    <td>{{ $item->bon_uang }}</td>
                                    <td>{{ $item->terlambat }}</td>
                                    <td>{{ $item->potongan_lain }}</td>
                                    <td>{{ $item->total }}</td>
                                    {{--                        <td><a href="{{ url('transaksi/penggajian/'.$item->eid) }}">{{ $item->total }}</a></td>--}}

                                    <td>
                                        <a href="{{ url('transaksi/penggajian/update/' . $item->eid) }}"
                                           class="btn btn-warning">Ubah</a>
                                        <a href="{{ url('transaksi/penggajian/'.$item->eid) }}" class="btn btn-primary">Detail</a>
                                        <a href="{{ url('transaksi/penggajian/print/'.$item->eid) }}" target="_blank"
                                           class="btn btn-success"><i class="fa fa-print"></i> Print</a>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#datatable').dataTable({
                "columnDefs": [
                    {
                        "targets": [0, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "order": [[0, "desc"]],
                dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'print',
                        text: 'Print table',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });

            $('.datepicker').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                }
            });
        });

        @if (session()->has('success'))
        swal(
            "{{ session('success') ? 'Sukses' : 'Gagal' }}",
            '{{ session("message") }}',
            "{{ session('success') ? 'success' : 'error' }}",
        )
        {{ session()->forget(['success', 'message']) }}
        {{ session()->save() }}
        @endif
    </script>
@endsection

@section('more-css')
    <style>
        .vtable .th1 {
            vertical-align : middle;
            text-align: center;
        }
    </style>
@endsection
