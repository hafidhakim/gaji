@extends('admin.master')

@section('content')

<div class="row page-header">
</div>

<section class="main-content">

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row margin-b-40">
                    <div class="col-sm-6">
                        <h4>{{ $payroll->pelanggan->name }}</h4>
                        <address>
                            Tanggal {{ date('Y-m-d', strtotime($payroll->date)) }}
                        </address>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset- 2">
                      <div><strong>Gaji</strong></div><br>
                      <table class="table table-striped text-right">
                          <tbody>
                              <tr>
                                  <td><strong>Gaji Pokok :</strong></td>
                                  <td>Rp. {{ $payroll->gaji_pokok }}</td>
                              </tr>
                              <tr>
                                  <td><strong>Tunjangan Jabatan :</strong></td>
                                  <td>Rp. {{ $payroll->tunjangan_jabatan }}  </td>
                              </tr>
                              <tr>
                                  <td><strong>Uang Makan :</strong></td>
                                  <td>Rp. {{ $payroll->uang_makan }}</td>
                              </tr>
                              <tr>
                                  <td><strong>Uang Premi :</strong></td>
                                  <td>Rp. {{ $payroll->uang_premi }}</td>
                              </tr>
                              <tr>
                                  <td><strong>Gaji Lain-lain :</strong></td>
                                  <td>Rp. {{ $payroll->gaji_lain }}</td>
                              </tr>
                          </tbody>
                      </table>
                    </div>
                    <div class="col-md-6 col-md-offset-2">
                      <div><strong>Potongan</strong></div><br>
                      <table class="table table-striped text-right">
                          <tbody>
                              <tr>
                                  <td><strong>Bon Barang :</strong></td>
                                  <td>Rp. {{ $payroll->bon_barang }}</td>
                              </tr>
                              <tr>
                                  <td><strong>Bon Uang :</strong></td>
                                  <td>Rp. {{ $payroll->bon_uang }}</td>
                              </tr>
                              <tr>
                                  <td><strong>Terlambat :</strong></td>
                                  <td>Rp. {{ $payroll->terlambat }}</td>
                              </tr>
                              <tr>
                                  <td><strong>Potongan Lain-lain :</strong></td>
                                  <td>Rp. {{ $payroll->potongan_lain }}</td>
                              </tr>
                          </tbody>
                      </table>
                      <div class="pull-right">
                        <h2><strong>Total : Rp. {{ $payroll->total }}</strong></h2>
                      </div>
                    </div>
                </div>

                <div class="row">
                  <div class="col-md-12 text-right">
                    <div>
                      <a href="{{ url('transaksi/penggajian/print/'.$payroll->id) }}" target="_blank" 
                        <button class="btn btn-success"><i class="fa fa-print"></i> Print</button>
                      </a>
                    </div>
                  </div>
                </div>

            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <span>Copyright &copy; 2018 FixedPlus</span>
</footer>


</section>
@endsection

@section('js')
<script>
function numberWithCurrency(x) {
  return "Rp " + x
    .toString()
    .replace(/\./g, ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ",-";
}

$(document).ready(function(){
  $('.datatable').dataTable({
      "order": [[ 0, "desc" ]]
  });

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('body').on('click', '.select-pelanggan', function(){
    var id = $(this).data('target');
    $('#pelanggan_id').val(id);
    $('#pelanggan-modal').modal('hide');

    return false;
  });

  function addItemDetail() {
    var countRow = $('.table-detail tbody tr').length;

    $.ajax({
      url: "{{ url('ajax/produk/item-html') }}",
      method: "GET",
      data: "countrow=" + countRow + "&price_custom=false",
      success: function(response) {
        $('.table-detail tbody').append(response);
      }
    });
  }

  var countRow = $('.table-detail tbody tr').length;
  if (countRow <= 0) {
    addItemDetail();
  }
  $('body').on('click', '.add-item', function(){
    addItemDetail();

    return false;
  });
  $('body').on('click', '.remove-item', function(){
    $(this).parents('tr').remove();

    return false;
  });

  var produkTargetRow = "";

  $('body').on('click', '.open-produk-modal', function () {
    produkTargetRow = $(this).data('target');
    $('#produk-modal').modal('show');
  });

  function loadPriceTotal() {
    var price_total = 0;
    $( ".table-detail tbody tr" ).each(function( index ) {
      price_total += parseInt($(this).children('td').eq(6).html().replace("Rp ", "").replace(",-", "").replace(/\./g, ""));
    });

    $('#price_total').html(numberWithCurrency(price_total));
  }

  $('body').on('click', '.select-produk', function(){
    var id = $(this).data('target');
    var kategori = $(this).parents('tr').children('td').eq(3).html();
    var harga = $(this).parents('tr').children('td').eq(4).html();
    var stok = $(this).parents('tr').children('td').eq(5).html();
    var satuan = $(this).parents('tr').children('td').eq(6).html();
    var qty = $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").val();
    $('.table-detail tbody tr').eq(produkTargetRow).find("select[name='produk_id[]']").val(id);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(1).html(kategori);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(2).html(numberWithCurrency(harga));
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(3).html(stok);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(4).html(satuan);
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(5).find("input").focus();
    $('.table-detail tbody tr').eq(produkTargetRow).children("td").eq(6).html(numberWithCurrency(qty * harga));
    $('#produk-modal').modal('hide');
    loadPriceTotal();

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="produk_price[]"]', function(){
    var harga = parseInt($(this).val());
    var hargaMinimum = parseInt($(this).attr('min'));
    var qty = $(this).parents('tr').children('td').eq(5).find('input').val();

    if (harga < hargaMinimum) {
      $(this).val(hargaMinimum);
    }else {
      $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
      loadPriceTotal();
    }

    return false;
  });

  $('body').on('change', '.table-detail tbody tr input[name="qty[]"]', function(){
    var harga = $(this).parents('tr').children('td').eq(2).html().replace("Rp ", "").replace(",-", "").replace(/\./g, "");
    var qty = $(this).val();
    $(this).parents('tr').children('td').eq(6).html(numberWithCurrency(qty * harga));
    loadPriceTotal();

    return false;
  });
});
@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection
