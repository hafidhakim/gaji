@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Komisi</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Transaksi Komisi</li>
    </ol>
  </div>
  <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('transaksi/komisi/create') }}" class="btn btn-success box-shadow btn-icon btn-rounded"><i class="fa fa-plus"></i> Buat Baru</a>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Transaksi Komisi
        <div class="pull-right">
          <form method="get" action="">
            <label for="date_range" class="pull-right">Filter & Print</label>
            <div class="input-group">
              <input type="text" name="date_range" id="date_range" class="form-control pull-right datepicker" style="min-width: 100px;" value="">
              <div class="input-group-append">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{ url('transaksi/komisi/prints?date_range='.$date_range) }}" target="blank_" class="btn btn-warning box-shadow btn-icon btn-rounded"><i class="fa fa-print"></i> Print Filtered</a>

              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="card-body">
        <table id="datatable" class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Karyawan</th>
                    <th>Tanggal</th>
                    <th>Total Komisi Tunai</th>
                    <th>Total Komisi Kredit</th>
                    <th>Grand Total</th>
                    <th width="100">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}
                        <td>{{ $item->pelanggan->name ? $item->pelanggan->name : '-' }}</td>
                        <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                        <td>{{ $item->komisi_tunai }}</td>
                        <td>{{ $item->komisi_kredit }}</td>
                        <td>{{ $item->grand_total }}</td>
                        <td>
                            <a href="{{ url('transaksi/komisi/update/' . $item->eid) }}" class="btn btn-warning">Ubah</a>
{{--                            <a href="{{ url('transaksi/komisi/'.$item->id) }}" class="btn btn-primary">Detail</a>--}}
                            <a href="{{ url('transaksi/komisi/print/'.$item->eid) }}" target="_blank" class="btn btn-success"><i class="fa fa-print"></i> Print</a>
                        </td>
                        
                    </tr>
                @endforeach

            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "columnDefs": [
            {
                "targets": [ 0,3,4 ],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[ 0, "desc" ]],
        dom: 'lBfrtip',
        buttons: [
        {
          extend: 'print',
          text: 'Print table',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'excel',
        exportOptions: {
            columns: [ 0, 1, 2, 3, 4, 5 ],
        }
        },
        {
          extend: 'pdf',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend: 'copy',
          exportOptions: {
            columns: ':visible'
          }
        },
          'colvis'
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });

    $('.datepicker').daterangepicker({
      locale: {
        format: 'YYYY/MM/DD'
      }
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection

