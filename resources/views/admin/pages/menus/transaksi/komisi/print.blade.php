<?php
error_reporting(E_ALL ^ E_DEPRECATED);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Slip Komisi Sales </title>
    <style>
        @page {
            margin: 10px;
        }

        .page-break {
            page-break-after: always;
        }

        td {
            font-size: 14px;
        }

        .center{
            margin: auto;
            text-align: center;
            margin-top:10px;
        }

        .total{
            margin: 0;
            text-align: right;
            font-weight: 900;
        }
    </style>
</head>
<body style="padding: 0 50px;">
<div style="text-align: center; margin-bottom: 25px;">
    <br>
    <strong style="font-size: 20px;">SLIP KOMISI SALES</strong>
    <br>
    <strong style="font-size: 20px;">ALDO ACCESSORIES</strong>
</div>
<div style="margin-bottom: 5px;">
    Karyawan : {{ $komisi->pelanggan->name }}
</div>
<div style="margin-bottom: 5px;">
    Divisi : {{ strtoupper($komisi->pelanggan->department->name) }}
</div>
<div style="margin-bottom: 10px;">
    Tanggal : {{ date('d F Y', strtotime($komisi->date)) }}
</div>
<hr>
<div style="margin-bottom: 5px; font-size: 14px;">
    <b>Omzet Tunai : {{ currencyFormat($komisi->omzet_tunai) }} {{-- x {{ ($komisi->pct_tunai) }}% --}} <br>
    <b>Omzet Kredit : {{ currencyFormat($komisi->omzet_kredit) }} {{-- x {{ ($komisi->pct_kredit) }}% --}}
</div>
<hr>
<table border="0" cellspacing="0" cellpadding="2" width="100%">
    <tbody>
    <tr>
        <td align="center" colspan="2">
            <span style="font-weight:bold; text-decoration: underline;">Komisi</span>
        </td>
        <td align="center" colspan="2">
            <span style="font-weight:bold; text-decoration: underline;">Potongan</span>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td><strong>Komisi Tunai </strong></td>
        <td> : {{ currencyFormat($komisi->komisi_tunai) }}</td>
        <td><strong>Potongan Stok Opname </strong></td>
        <td> : {{ currencyFormat($komisi->potongan_opname) }}</td>
    </tr>
    <tr>
        <td><strong>Komisi Kredit </strong></td>
        <td> : {{ currencyFormat($komisi->komisi_kredit) }}  </td>
        <td><strong>Bon Uang </strong></td>
        <td> : {{ currencyFormat($komisi->bon_uang) }}</td>
    </tr>
    <tr>
        <td><strong>Bonus </strong></td>
        <td> : {{ currencyFormat($komisi->bonus) }}</td>
        <td><strong>Potongan Lain-lain </strong></td>
        <td> : {{ currencyFormat($komisi->potongan_lain) }}</td>
    </tr>
    <tr>
        <td><strong>Komisi Lain-lain </strong></td>
        <td> : {{ currencyFormat($komisi->komisi_lain) }}</td>
    </tr>
    </tbody>
</table>
<br>
<hr style="margin: 0em;border-width: 2px;">
<div class="total">
    Total : {{ currencyFormat($komisi->grand_total) }}
</div>
<hr style="margin: 0em;border-width: 2px;">
<div class="center"><?php echo "Sidoarjo, " . date("d F Y");?></div>
<br>
<div style="text-align: left;">
    Hormat Kami,
    <br>
    <br>
    <br>
    HRD
</div>
</body>
</html>