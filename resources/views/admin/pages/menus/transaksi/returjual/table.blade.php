@extends('admin.master')

@section('content')
<div class="row page-header">
  <div class="col-lg-6 align-self-center ">
    <h2>Retur Jual</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item active">Transaksi Retur Jual</li>		
    </ol>
  </div>
  <div class="col-lg-6 align-self-center text-right">
    <a href="{{ url('transaksi/returjual/create') }}" class="btn btn-success box-shadow btn-icon btn-rounded"><i class="fa fa-plus"></i> Buat Baru</a>
  </div>
</div>

<section class="main-content">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Transaksi Retur Jual
      </div>
      <div class="card-body">
        <table id="datatable" class="table table-striped dt-responsive wrap">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Code</th>
                    <th>Code Penjualan</th>
                    <th>Nama Pelanggan</th>
                    <th>Tanggal</th>
                    <th>Total Harga</th>
                    <th width="100">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->code }}</td>
                        <td>{{ $item->penjualan ? $item->penjualan->code : '-' }}</td>
                        <td>{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->name : '-' }}</td>
                        <td>{{ date('Y-m-d', strtotime($item->date)) }}</td>
                        <td>{{ currencyFormat($item->price_total) }}</td>
                        <td>
                            <a href="{{ url('transaksi/returjual/'.$item->id) }}" class="btn btn-primary">Detail</a>
                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#datatable').dataTable({
        "order": [[ 0, "desc" ]]
    });
});

@if (session()->has('success'))
    swal(
      "{{ session('success') ? 'Sukses' : 'Gagal' }}",
      '{{ session("message") }}',
      "{{ session('success') ? 'success' : 'error' }}",
    )
    {{ session()->forget(['success', 'message']) }}
    {{ session()->save() }}
@endif
</script>
@endsection