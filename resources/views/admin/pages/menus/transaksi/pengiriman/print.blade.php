<html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Perintah Pengiriman</title>
  <style>
  @page { margin: 20px; }
  .page-break {
    page-break-after: always;
  }
  </style>
</head><body style="padding: 0 50px;">
<br>
<br>
<br>
<br>
<br>
<div style="text-align: center; margin-bottom: 25px;">
<strong style="font-size: 24px;">PERINTAH PENGIRIMAN</strong>
</div>
<div style="margin-bottom: 5px;">
Dengan kode pengiriman <strong>{{ $pengiriman->code }}</strong>
</div>
<div style="margin-bottom: 5px;">
Tanggal pengiriman: <strong>{{ date("d F Y", strtotime($pengiriman->date)) }}</strong>
</div>
<div style="margin-bottom: 10px;">
Berikut transaksi penjualan yang ingin dikirimkan ke pelanggan
</div>
<table border="1" cellspacing="0" cellpadding="10" width="100%">
    <thead>
    <tr>
        <td align="center">
        <strong>Penjualan</strong>
        </td>
        <td align="center">
        <strong>Pelanggan</strong>
        </td>
        <td align="center">
        <strong>No. HP</strong>
        </td>
        <td align="center">
        <strong>Alamat Tujuan</strong>
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($pengiriman->detail as $item)
    <tr>
        <td>{{ $item->penjualan->code }}</td>
        <td align="center">{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->name : '-' }}</td>
        <td align="center">{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->phone : '-' }}</td>
        <td align="center">{{ $item->penjualan->pelanggan ? $item->penjualan->pelanggan->address : '-' }}</td>
    </tr>
    @endforeach
    </tbody>
</table>
<div style="margin-top: 10px;">
Terima kasih.
</div>
<div style="text-align: right;margin-top: 20px;">
Hormat Kami,
<br>
Admin Yabes Optical
</div>
</body></html>
