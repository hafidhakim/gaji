@extends('admin.master')

@section('content')
@endsection

@section('js')
<!--Chart Script-->
<script src="{{asset('assets/fixed-plus/lib/chartjs/chart.min.js') }}"></script>
<script src="{{asset('assets/fixed-plus/lib/chartjs/chartjs-sass.js') }}"></script>

<!--Vetor Map Script-->
<script src="{{asset('assets/fixed-plus/lib/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{asset('assets/fixed-plus/lib/vectormap/jquery-jvectormap-us-aea-en.js') }}"></script>

<!-- Chart C3 -->
<script src="{{asset('assets/fixed-plus/lib/chart-c3/d3.min.js') }}"></script>
<script src="{{asset('assets/fixed-plus/lib/chart-c3/c3.min.js') }}"></script>

<!-- Datatables-->
<script src="{{asset('assets/fixed-plus/lib/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('assets/fixed-plus/lib/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{asset('assets/fixed-plus/lib/toast/jquery.toast.min.js') }}"></script>
<script src="{{asset('assets/fixed-plus/js/dashboard.js') }}"></script>
@endsection
