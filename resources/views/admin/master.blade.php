<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aldo Accs - Penggajian</title>

        <!-- Common Plugins -->
        <link href="{{asset('assets/fixed-plus/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

		<!-- Vector Map Css-->
        <link href="{{asset('assets/fixed-plus/lib/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />

		<!-- Chart C3 -->
		<link href="{{asset('assets/fixed-plus/lib/chart-c3/c3.min.css') }}" rel="stylesheet">
		<link href="{{asset('assets/fixed-plus/lib/chartjs/chartjs-sass-default.css') }}" rel="stylesheet">

        <!-- DataTimePicker -->
        <link href="{{asset('assets/fixed-plus/lib/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

		<!-- Clock Picker -->
		<link href="{{asset('assets/fixed-plus/lib/clockpicker/bootstrap-clockpicker.min.css') }}" rel="stylesheet">

        <!-- DataTables -->
        <link href="{{asset('assets/fixed-plus/lib/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/fixed-plus/lib/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/fixed-plus/lib/toast/jquery.toast.min.css') }}" rel="stylesheet">
		    <link href="{{asset('assets/fixed-plus/lib/datatables/buttons.dataTables.css') }}" rel="stylesheet" type="text/css">

        <!-- Sweet Alerts -->
        <link href="{{asset('assets/fixed-plus/lib/sweet-alerts2/sweetalert2.min.css') }}" rel="stylesheet">

        <!-- Select2 -->
        <link href="{{asset('assets/select2/select2.min.css') }}" rel="stylesheet">

        <!-- Custom Css-->
        <link href="{{asset('assets/fixed-plus/css/style.css') }}" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            select[readonly]{
                background: #eee;
                cursor:no-drop;
            }

            select[readonly] option{
                display:none;
            }
        </style>
    </head>
    <body class="horizontal">
        @include('admin.partials.header')
        @include('admin.partials.menu')

		@yield('content')

        @include('admin.partials.footer')

        <!-- Common Plugins -->
        <script src="{{asset('assets/fixed-plus/lib/jquery/dist/jquery.min.js') }}"></script>
		<script src="{{asset('assets/fixed-plus/lib/bootstrap/js/popper.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/bootstrap/js/bootstrap.min.js') }}"></script>
		<script src="{{asset('assets/fixed-plus/lib/ace-menu/ace-responsive-menu-min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/pace/pace.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/nano-scroll/jquery.nanoscroller.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/metisMenu/metisMenu.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/js/custom.js') }}"></script>

        <!--Sweet Alerts-->
        <script src="{{asset('assets/fixed-plus/lib/sweet-alerts2/sweetalert2.min.js') }}"></script>

        <!-- Select2 -->
        <script src="{{asset('assets/select2/select2.min.js') }}"></script>

        <!-- DataTimePicker -->
        <script src="{{asset('assets/fixed-plus/lib/bootstrap-daterangepicker/moment.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

		<!-- Clock Picker -->
		<script type="text/javascript" src="{{asset('assets/fixed-plus/lib/clockpicker/bootstrap-clockpicker.min.js') }}"></script>

        <!-- Datatables-->
        <script src="{{asset('assets/fixed-plus/lib/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/datatables/dataTables.responsive.min.js') }}"></script>

		<script src="{{asset('assets/fixed-plus/lib/datatables/dataTables.buttons.min.js') }}"></script>
		<script src="{{asset('assets/fixed-plus/lib/datatables/jszip.min.js') }}"></script>
		<script src="{{asset('assets/fixed-plus/lib/datatables/pdfmake.min.js') }}"></script>
		<script src="{{asset('assets/fixed-plus/lib/datatables/vfs_fonts.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/datatables/buttons.print.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/datatables/buttons.colVis.min.js') }}"></script>
        <!-- Jquery UI -->
        <script src="{{asset('assets/fixed-plus/lib/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/js/jquery.ui.custom.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/js/jquery.inputmask.bundle.js') }}"></script>

        <script>
        $(document).ready(function(){
            $('.select2').select2();
        });
        </script>

        @yield('js')
        @yield('more-js','')
        @yield('more-css','')
    </body>
</html>
