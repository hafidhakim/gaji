<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aldo Accs - Penggajian</title>

        <!-- Common Plugins -->
        <link href="{{asset('assets/fixed-plus/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Custom Css-->
        <link href="{{asset('assets/fixed-plus/css/style.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            html,body{
                height: 100%;
            }
        </style>
    </head>
    <body class="bg-light">

        <div class="misc-wrapper">
            <div class="misc-content">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-4">
                              <div class="misc-header text-center">
								<h2>Aldo Accs</h2>
                            </div>
                            <div class="misc-box">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                        <label for="username">Username</label>
                                        <div class="group-icon">
                                        <input id="username" name="username" type="text" placeholder="Username" class="form-control" required autofocus>
                                        <span class="icon-user text-muted icon-input"></span>
                                        </div>
                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">Kata Sandi</label>
                                        <div class="group-icon">
                                        <input id="password" name="password" type="password" placeholder="Kata Sandi" class="form-control" required>
                                        <span class="icon-lock text-muted icon-input"></span>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    @if (session()->has('message'))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>{{ session('message') }}</strong>
                                        </div>
                                    @endif
                                    <div class="clearfix">
                                        <button type="submit" class="btn btn-block btn-primary btn-rounded box-shadow">Masuk</button>
                                    </div>
                                </form>
                                <hr>
                                <div class="text-center misc-footer">
                                    <p>Hak Cipta &copy; {{ date('Y') }} Aldo Accs</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Common Plugins -->
        <script src="{{asset('assets/fixed-plus/lib/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/js/popper.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/pace/pace.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/nano-scroll/jquery.nanoscroller.min.js') }}"></script>
        <script src="{{asset('assets/fixed-plus/lib/metisMenu/metisMenu.min.js') }}"></script>
    </body>
</html>
