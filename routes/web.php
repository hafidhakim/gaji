<?php
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth', 'preventBackHistory']], function(){
    Route::get('/', 'DashboardController@index');

    //Satuan =============================================================================
    Route::get('master/satuan', 'SatuanController@index');
    Route::get('master/satuan/create', 'SatuanController@create');
    Route::post('master/satuan/create', 'SatuanController@StoreNew');
    Route::get('master/satuan/{id}', 'SatuanController@edit');
    Route::post('master/satuan/{id}', 'SatuanController@update');
    Route::get('master/satuan/delete/{id}', 'SatuanController@delete');
    //====================================================================================

    //Kategori ===========================================================================
    Route::get('master/kategori', 'KategoriController@index');
    Route::get('master/kategori/create', 'KategoriController@create');
    Route::post('master/kategori/create', 'KategoriController@StoreNew');
    Route::get('master/kategori/{id}', 'KategoriController@edit');
    Route::post('master/kategori/{id}', 'KategoriController@update');
    Route::get('master/kategori/delete/{id}', 'KategoriController@delete');
    //====================================================================================

    //Department ===========================================================================
    Route::get('master/department', 'DepartmentController@index');
    Route::get('master/department/create', 'DepartmentController@create');
    Route::post('master/department/create', 'DepartmentController@StoreNew');
    Route::get('master/department/{id}', 'DepartmentController@edit');
    Route::post('master/department/{id}', 'DepartmentController@update');
    Route::get('master/department/delete/{id}', 'DepartmentController@delete');
    //====================================================================================

    //Produk =============================================================================
    Route::get('master/produk', 'ProdukController@index');
    Route::get('master/produk/create', 'ProdukController@create');
    Route::post('master/produk/create', 'ProdukController@StoreNew');
    Route::get('master/produk/{id}', 'ProdukController@edit');
    Route::post('master/produk/{id}', 'ProdukController@update');
    Route::get('master/produk/delete/{id}', 'ProdukController@delete');

    Route::get('ajax/produk/item-html', 'ProdukController@itemHTML');
    //====================================================================================

    //Pengguna ===========================================================================
    Route::get('master/pengguna', 'PenggunaController@index');
    Route::get('master/pengguna/create', 'PenggunaController@create');
    Route::post('master/pengguna/create', 'PenggunaController@StoreNew');
    Route::get('master/pengguna/{id}', 'PenggunaController@edit');
    Route::post('master/pengguna/{id}', 'PenggunaController@update');
    Route::get('master/pengguna/delete/{id}', 'PenggunaController@delete');
    //====================================================================================

    //Pelanggan ==========================================================================
    Route::get('master/karyawan', 'PelangganController@index');
    Route::get('master/karyawan/create', 'PelangganController@create');
    Route::post('master/karyawan/create', 'PelangganController@StoreNew');
    Route::get('master/karyawan/{id}', 'PelangganController@edit');
    Route::post('master/karyawan/{id}', 'PelangganController@update');
    Route::get('master/karyawan/delete/{id}', 'PelangganController@delete');
    //====================================================================================

    //Supplier ===========================================================================
    Route::get('master/supplier', 'SupplierController@index');
    Route::get('master/supplier/create', 'SupplierController@create');
    Route::post('master/supplier/create', 'SupplierController@StoreNew');
    Route::get('master/supplier/{id}', 'SupplierController@edit');
    Route::post('master/supplier/{id}', 'SupplierController@update');
    Route::get('master/supplier/delete/{id}', 'SupplierController@delete');
    //====================================================================================

    //Absensi =============================================================================
    Route::get('absensi', 'AbsensiController@index');
    Route::get('absensi/create', 'AbsensiController@create');
    Route::post('absensi/create', 'AbsensiController@StoreNew');
    Route::get('absensi/approve/{id}', 'AbsensiController@approve');
    Route::get('absensi/reject/{id}', 'AbsensiController@reject');
    //====================================================================================

    //Transaksi Pembelian ================================================================
    Route::get('transaksi/home', 'PembelianController@home');
    Route::get('transaksi/pembelian', 'PembelianController@index');
    Route::get('transaksi/pembelian/create', 'PembelianController@create');
    Route::post('transaksi/pembelian/create', 'PembelianController@StoreNew');
    Route::get('transaksi/pembelian/{id}', 'PembelianController@show');
    Route::post('transaksi/pembelian/{id}', 'PembelianController@update');
    Route::get('transaksi/pembelian/print/{id}', 'PembelianController@print');
    Route::get('transaksi/pembelian/{id}/bayar', 'PembelianController@bayar');
    Route::post('transaksi/pembelian/{id}/bayar', 'PembelianController@bayarSave');

    //Transaksi Pembayaran ================================================================
    Route::get('transaksi/pembayaran', 'PembayaranController@index');
    Route::get('transaksi/pembayaran/create', 'PembayaranController@create');
    Route::post('transaksi/pembayaran/create', 'PembayaranController@StoreNew');
    Route::get('transaksi/pembayaran/{id}', 'PembayaranController@show');
    Route::post('transaksi/pembayaran/{id}', 'PembayaranController@update');
    Route::get('transaksi/pembayaran/print/{id}', 'PembayaranController@print');
    //====================================================================================

    //Transaksi Penjualan ================================================================
    Route::get('transaksi/penjualan', 'PenjualanController@index');
    Route::get('transaksi/penjualan/create', 'PenjualanController@create');
    Route::post('transaksi/penjualan/create', 'PenjualanController@StoreNew');
    Route::get('transaksi/penjualan/{id}', 'PenjualanController@show');
    Route::post('transaksi/penjualan/{id}', 'PenjualanController@update');
    Route::get('transaksi/penjualan/print/{id}', 'PenjualanController@print');
    Route::get('transaksi/penjualan/{id}/bayar', 'PenjualanController@bayar');
    Route::post('transaksi/penjualan/{id}/bayar', 'PenjualanController@bayarSave');

    Route::get('ajax/penjualan/item-html', 'PenjualanController@itemHTML');
    //====================================================================================

    //Transaksi Penggajian ================================================================
    Route::get('transaksi/penggajian', 'PenggajianController@index');
    Route::get('transaksi/penggajian/create', 'PenggajianController@create');
    Route::post('transaksi/penggajian/create', 'PenggajianController@StoreNew');
    Route::get('transaksi/penggajian/prints', 'PenggajianController@prints');
    Route::get('transaksi/penggajian/{id}', 'PenggajianController@show');
    Route::get('transaksi/penggajian/update/{id}', 'PenggajianController@edit');
    Route::post('transaksi/penggajian/update/{id}', 'PenggajianController@update');
    Route::get('transaksi/penggajian/print/{id}', 'PenggajianController@print');
    Route::get('transaksi/penggajian/{id}/bayar', 'PenggajianController@bayar');
    Route::post('transaksi/penggajian/{id}/bayar', 'PenggajianController@bayarSave');

    Route::get('ajax/penggajian/item-html', 'PenggajianController@itemHTML');
    //====================================================================================

    //Transaksi Komisi ================================================================
    Route::get('transaksi/komisi', 'KomisiController@index');
    Route::get('transaksi/komisi/create', 'KomisiController@create');
    Route::post('transaksi/komisi/create', 'KomisiController@StoreNew');
    Route::get('transaksi/komisi/prints', 'KomisiController@prints');
    Route::get('transaksi/komisi/{id}', 'KomisiController@show');
    Route::get('transaksi/komisi/update/{id}', 'KomisiController@edit');
    Route::post('transaksi/komisi/update/{id}', 'KomisiController@update');
    Route::get('transaksi/komisi/print/{id}', 'KomisiController@print');
    Route::get('transaksi/komisi/{id}/bayar', 'KomisiController@bayar');
    Route::post('transaksi/komisi/{id}/bayar', 'KomisiController@bayarSave');

    Route::get('ajax/komisi/item-html', 'KomisiController@itemHTML');
    //====================================================================================

    //Transaksi Retur Jual ================================================================
    Route::get('transaksi/returjual', 'ReturJualController@index');
    Route::get('transaksi/returjual/create', 'ReturJualController@create');
    Route::post('transaksi/returjual/create', 'ReturJualController@StoreNew');
    Route::get('transaksi/returjual/{id}', 'ReturJualController@show');
    Route::get('transaksi/returjual/print/{id}', 'ReturJualController@print');
    Route::get('transaksi/returjual/penjualan-detail/{id}', 'ReturJualController@penjualanDetail');
    //====================================================================================

    //Transaksi Penerimaan ================================================================
    Route::get('transaksi/penerimaan', 'PenerimaanController@index');
    Route::get('transaksi/penerimaan/create', 'PenerimaanController@create');
    Route::post('transaksi/penerimaan/create', 'PenerimaanController@StoreNew');
    Route::get('transaksi/penerimaan/{id}', 'PenerimaanController@show');
    Route::get('transaksi/penerimaan/print/{id}', 'PenerimaanController@print');
    Route::get('transaksi/penerimaan/pembelian-detail/{id}', 'PenerimaanController@pembelianDetail');
    //====================================================================================

    //Transaksi Pengiriman ================================================================
    Route::get('transaksi/pengiriman', 'PengirimanController@index');
    Route::get('transaksi/pengiriman/create', 'PengirimanController@create');
    Route::post('transaksi/pengiriman/create', 'PengirimanController@StoreNew');
    Route::get('transaksi/pengiriman/{id}', 'PengirimanController@show');
    Route::post('transaksi/pengiriman/{id}', 'PengirimanController@update');
    Route::get('transaksi/pengiriman/print/{id}', 'PengirimanController@print');
    Route::post('transaksi/pengiriman/{id}/terkirim', 'PengirimanController@terkirim');
    //====================================================================================

    //Report Laba Rugi ================================================================
    Route::get('laporan/labarugi', 'ReportLabaRugiController@index');
    Route::get('laporan/labarugi/print', 'ReportLabaRugiController@print');
    //====================================================================================

    //Report Penjualan ================================================================
    Route::get('laporan/penjualan', 'ReportPenjualanController@index');
    Route::get('laporan/penjualan/print', 'ReportPenjualanController@print');
    //====================================================================================

    //Report ReturJual ================================================================
    Route::get('laporan/returjual', 'ReportReturJualController@index');
    Route::get('laporan/returjual/print', 'ReportReturJualController@print');
    //====================================================================================

    //Report Pembelian ================================================================
    Route::get('laporan/pembelian', 'ReportPembelianController@index');
    Route::get('laporan/pembelian/print', 'ReportPembelianController@print');
    //====================================================================================

    //Report Penerimaan ================================================================
    Route::get('laporan/penerimaan', 'ReportPenerimaanController@index');
    Route::get('laporan/penerimaan/print', 'ReportPenerimaanController@print');
    //====================================================================================

    //Report Pembayaran ================================================================
    Route::get('laporan/pembayaran', 'ReportPembayaranController@index');
    Route::get('laporan/pembayaran/print', 'ReportPembayaranController@print');
    //====================================================================================

    //Report Pengiriman ================================================================
    Route::get('laporan/pengiriman', 'ReportPengirimanController@index');
    Route::get('laporan/pengiriman/print', 'ReportPengirimanController@print');
    //====================================================================================
});
