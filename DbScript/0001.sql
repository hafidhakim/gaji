ALTER TABLE `komisi`
CHANGE COLUMN `komisi_tunai` `omzet_tunai` INT(11) NULL DEFAULT 0 ,
CHANGE COLUMN `total_tunai` `komisi_tunai` INT(11) NULL DEFAULT 0 ,
CHANGE COLUMN `komisi_kredit` `omzet_kredit` INT(11) NULL DEFAULT 0 ,
CHANGE COLUMN `total_kredit` `komisi_kredit` INT(11) NULL DEFAULT 0 ;

alter table komisi
    add column if not exists bonus INT(11) not null default 0 after komisi_kredit;
alter table komisi
    add column if not exists komisi_lain INT(11) not null default 0 after bonus;
alter table komisi
    add column if not exists bon_uang INT(11) not null default 0 after komisi_lain;
alter table komisi
    add column if not exists potongan_opname INT(11) not null default 0 after bon_uang;
alter table komisi
    add column if not exists potongan_lain INT(11) not null default 0 after potongan_opname;
