<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pelanggan;
use App\Models\Department;
use Response;
use Illuminate\Support\Facades\Crypt;


class PelangganController extends Controller
{
  public function index()
  {
    // $id = Pelanggan::find('id');
    // $ids = Crypt::encryptString($id);
    $data['data'] = Pelanggan::orderBy('id', 'desc')->get();
    // return $data;
    // <a href="'.route('entry.bank', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
    // $id = Crypt::decryptString($request->input('i'));


    return view('admin.pages.menus.master.pelanggan.table', $data);
  }

  public function create()
  {
    $data['department'] = Department::all();
    $data['newcode'] = Pelanggan::orderBy('id', 'desc')->first();
    if (!$data['newcode']) {
      $data['newcode'] = "KRY1";
    }else {
      $data['newcode'] = "KRY" . ($data['newcode']->id + 1);
    }

    return view('admin.pages.menus.master.pelanggan.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
      'code' => 'required|unique:pelanggan|max:20',
      'name' => 'required|max:30',
      'gaji_pokok' => 'required',
      'phone' => 'required|max:42',
      'address' => 'required',
    ]);

    $req['name'] = strtoupper($req['name']);
    $req['address'] = strtoupper($req['address']);

    $pelanggan = Pelanggan::create($req);

    if ($pelanggan) {
      return redirect('/master/karyawan')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/karyawan')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $data['data'] = Pelanggan::find($id);
    $data['department'] = Department::all();

    return view('admin.pages.menus.master.pelanggan.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token');
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $this->validate($request, [
      'name' => 'required|max:30',
      'gaji_pokok' => 'required',
      'phone' => 'required|max:42',
      'address' => 'required',
    ]);

    $req['name'] = strtoupper($req['name']);
    $req['address'] = strtoupper($req['address']);

    // return $req;
    $pelanggan = Pelanggan::where('id', $id)->update($req);

    if ($pelanggan) {
      // return redirect($request->url())->with('message', 'Barang Catagory '.$nama.' berhasil didaftarkan');with('message', 'Barang Catagory '.$nama.' berhasil didaftarkan');
      return redirect('/master/karyawan')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/karyawan')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function delete($id)
  {
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $pelanggan = Pelanggan::find($id);
    $pelanggan->delete();

    if ($pelanggan) {
      return redirect('/master/karyawan')->with([
        'success' => true,
        'message' => 'Berhasil menghapus data',
      ]);
    }

    return redirect('/master/karyawan')->with([
      'success' => false,
      'message' => 'Gagal menghapus data',
    ]);
  }
}
