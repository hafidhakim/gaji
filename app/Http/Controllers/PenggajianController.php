<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payroll;
use App\Models\Pelanggan;
use App\Models\User;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Auth;

class PenggajianController extends Controller
{
    public function index(Request $request)
    {
        $req = $request->all();
        $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
        if (isset($req['date_range'])) {
            $data['date_range'] = $req['date_range'];
        }
        $date_array = explode(" - ", $data['date_range']);
        $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
        $data['date_start'] = $date_array[0];
        $data['date_end'] = $date_array[1];

        $data['data'] = Payroll::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pelanggan')->orderBy('id', 'desc')->get();
        // return $data;
        return view('admin.pages.menus.transaksi.penggajian.table', $data);
    }

    public function create(Request $request)
    {
        $data['pelanggan'] = Pelanggan::orderBy('id', 'desc')->get();

        return view('admin.pages.menus.transaksi.penggajian.entry', $data);
    }

    public function StoreNew(Request $request)
    {
        // $total = 0;

        $req = $request->except('_token');
        // return $req;
        $date = Carbon::createFromFormat('Y-m-d', $req['date'])->format('m');
        // $date = $date->format('m');
        $isTransExists = Payroll::where('pegawai_id', $req['pegawai_id'])->whereMonth('date', $date)->count();
        // $isTransExists = Payroll::whereMonth('date', '11')->get();
        // return $isTransExists;
        $validator = $this->validate($request, [
            'pegawai_id' => 'required',
            'gaji_pokok' => 'required',
            'uang_makan' => 'required|numeric',
        ]);
        if ($validator and $validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $req['total'] = $req['gaji_pokok'] + $req['tunjangan_jabatan'] + $req['uang_makan'] + $req['uang_premi'] + $req['gaji_lain']
            - $req['bon_barang'] - $req['bon_uang'] - $req['terlambat'] - $req['potongan_lain'];
        // $req['date'] = Carbon::now()->format('m');

        if ($isTransExists > 0) {
            return redirect()->back()->with([
                'success' => false,
                'message' => 'Data Gaji Untuk Periode ' . date('F', strtotime($req['date'])) . ' Sudah Ada',
            ]);
        }
        // return $req;
        $payroll = Payroll::create($req);


        if ($payroll) {
            return redirect('/transaksi/penggajian')->with([
                'success' => true,
                'message' => 'Berhasil menyimpan data',
            ]);
        }

        return redirect('/master/produk')->with([
            'success' => false,
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function edit($id)
    {
        $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        $id = $encrypter->decrypt($id);
        $data['payroll'] = Payroll::find($id);
        $data['pelanggan'] = Pelanggan::orderBy('id', 'desc')->get();
        // return $data;

        return view('admin.pages.menus.transaksi.penggajian.edit', $data);
    }

    public function update($id, Request $request)
    {
        $req = $request->except('_token', 'password');
        $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        $id = $encrypter->decrypt($id);
        $password = $request->input('password');
        $hashPassword = User::find(Auth::id());
        if (!Hash::check($request->input('password'), $hashPassword->password)) {
            $error = 'Autentikasi Password Diperlukan';
            // return $error;
            return redirect()->back()->with('error', $error)->withInput();
        }
        $validator = $this->validate($request, [
            'date' => 'required',
            'pegawai_id' => 'required',
        ]);
        if ($validator and $validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $req['total'] = $req['gaji_pokok'] + $req['tunjangan_jabatan'] + $req['uang_makan'] + $req['uang_premi'] + $req['gaji_lain']
            - $req['bon_barang'] - $req['bon_uang'] - $req['terlambat'] - $req['potongan_lain'];
        // return $req;

        $payroll = Payroll::where('id', $id)->update($req);

        if ($payroll) {
            return redirect('/transaksi/penggajian')->with([
                'success' => true,
                'message' => 'Berhasil menyimpan data',
            ]);
        }

        return redirect('/master/produk')->with([
            'success' => false,
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function show($id)
    {
        $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        $id = $encrypter->decrypt($id);
        $data['payroll'] = Payroll::with('pelanggan')->find($id);
        // return $data;

        return view('admin.pages.menus.transaksi.penggajian.detail', $data);
    }

    public function print($id)
    {
        $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        $id = $encrypter->decrypt($id);
        $data['payroll'] = Payroll::with('pelanggan.department')->find($id);
//     return $data;
        $pdf = \PDF::loadView('admin.pages.menus.transaksi.penggajian.print', $data)->setPaper('a5', 'landscape');
        return $pdf->stream('payroll_' . $id . '.pdf');
    }

    public function prints(Request $request)
    {
        $req = $request->all();
        // return $req;
        // $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        // $id = $encrypter->decrypt($id);
        $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
        if (isset($req['date_range'])) {
            $data['date_range'] = $req['date_range'];
        }
        $date_array = explode(" - ", $data['date_range']);
        $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
        $data['date_start'] = $date_array[0];
        $data['date_end'] = $date_array[1];

        $data['payroll'] = Payroll::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pelanggan.department')->get();
        // return $data;

        $pdf = \PDF::loadView('admin.pages.menus.transaksi.penggajian.prints', $data)->setPaper('a5', 'landscape');
        return $pdf->stream('payroll_' . $data['date_start'] . '_' . $data['date_end'] . '.pdf');
    }
}
