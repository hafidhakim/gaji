<?php

namespace App\Http\Controllers\Master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Suplier;
use Response;

class SupplierController extends Controller
{
  public function index()
  {
      return view('admin.pages.menus.master.supplier.entry');
  }

  public function StoreNew(Request $request)
  {
    $name = $request->input('name');
    $phone = $request->input('phone');
    $address = $request->input('address');
    $suplier = new Suplier();
    $suplier->code = 'SUPCoba';
    $suplier->name = $name;
    $suplier->phone = $phone;
    $suplier->address = $address;
    $suplier->save();
    return redirect('/');
  }
}
