<?php

namespace App\Http\Controllers\Master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kategori;
use Response;

class KategoriController extends Controller
{
  public function index()
  {
      return view('admin.pages.menus.master.kategori.entry');
  }

  public function StoreNew(Request $request)
  {
    $name = $request->input('nama');
    $kategori = new Kategori();
    $kategori->name = $name;
    $kategori->save();
    return redirect('/');
  }
}
