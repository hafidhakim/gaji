<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penjualan;
use App\Models\PenjualanDetail;
use App\Models\Pelanggan;
use App\Models\Produk;
use App\Models\Returjual;
use App\Models\ReturjualDetail;
use Response;

class ReturJualController extends Controller
{
  	public function index()
  	{
  	  $data['data'] = Returjual::with('penjualan')->orderBy('id', 'desc')->get();
	
	  	  return view('admin.pages.menus.transaksi.returjual.table', $data);
  	}

  	public function create(Request $request)
    {

      $data['penjualan'] = Penjualan::doesnthave('returjual')->orderBy('id', 'desc')->get();
      $data['produk'] = Produk::orderBy('id', 'desc')->get();

      return view ('admin.pages.menus.transaksi.returjual.entry', $data);
    }

  	public function penjualanDetail($penjualanID)
    {
      $data['penjualan_detail'] = PenjualanDetail::where('penjualan_id', $penjualanID)->get();

      return view ('admin.pages.menus.transaksi.returjual.penjualan-detail', $data);
    }

    public function StoreNew(Request $request)
    {
      $req = $request->except('_token');
      $validator = $this->validate($request, [
        'date' => 'required',
        'penjualan_id' => 'required',
      ]);
      if ($validator and $validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
      }
  
      \DB::beginTransaction();
  
      try {
        $produk_idArray = $req['produk_id'];
        $qtyArray = $req['qty'];
        $index = 0;
        $price_total = 0;
        $returjual_detail = array();
        foreach($produk_idArray as $produkID) {
          $produk = Produk::find($produkID);
    
          if ($produk) {
            $produk_id = $produk->id;
            $produk_name = $produk->name;
            $qty = $qtyArray[$index];
            $produk_price = $produk->price_sell;
            $price_subtotal = $qty * $produk_price;
    
            if ($qty > 0) {
              $returjual_detail[] = array(
                'produk_id' => $produk_id,
                'produk_name' => $produk_name,
                'qty' => $qty,
                'produk_price' => $produk_price,
                'price_subtotal' => $price_subtotal,
              );
      
              // SUM for total
              $price_total += $price_subtotal;

              // Update stok produk
              $produk->stock = $produk->stock + $qty;
              $produk->save();
      
              $index++;
            }
          }
        }
  
        if (count($returjual_detail) <= 0) {
          return redirect()->back()->with([
            'success' => false,
            'message' => 'Produk harus diisi minimal 1 item',
          ])->withInput();
        }
  
        $code = Returjual::orderBy('code', 'desc')->first();
        if (!$code) {
          $req['code'] = "RETJUAL1";
        }else {
          $code = $code->code;
          $code = str_replace("RETJUAL", "", $code);
          $req['code'] = "RETJUAL" . ($code + 1);
        }
        $req['pengguna_id'] = auth()->user()->id;
        $req['price_total'] = $price_total;
        $returjual = Returjual::create($req);
  
        foreach($returjual_detail as $item) {
          $item['returjual_id'] = $returjual->id;
          ReturjualDetail::create($item);
        }
  
        \DB::commit();
        // all good
    
        if ($returjual) {
          return redirect('/transaksi/returjual/'.$returjual->id)->with([
            'success' => true,
            'message' => 'Berhasil menyimpan data',
          ]);
        }
      } catch (\Exception $e) {
        \DB::rollback();
        // something went wrong
        return redirect('/transaksi/returjual')->with([
          'success' => false,
          'message' => 'Gagal menyimpan data .' . $e->getMessage(),
        ]);
      }
    }
  
    public function show($id)
    {
      $data['returjual'] = Returjual::with('detail')->find($id);
      $data['penjualan'] = Penjualan::orderBy('id', 'desc')->get();
      $data['produk'] = Produk::orderBy('id', 'desc')->get();
  
      return view ('admin.pages.menus.transaksi.returjual.detail', $data);
    }

    public function print($id)
    {
      $data['returjual'] = Returjual::with('detail','penjualan')->find($id);
  
      $pdf = \PDF::loadView('admin.pages.menus.transaksi.returjual.print', $data);
      return $pdf->stream('returjual_'.$id.'.pdf');
    }
}
