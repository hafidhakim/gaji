<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Menu;
use App\Models\Hakakses;
use Response;

class PenggunaController extends Controller
{
  public function index()
  {
    $data['data'] = User::orderBy('id', 'desc')->get();
    // return $data;

    return view('admin.pages.menus.master.pengguna.table', $data);
  }

  public function create()
  {
    $menu_master = Menu::where('group', 'LIKE', 'master%')->get();
    $menu_transaksi = Menu::where('group', 'LIKE', 'transaksi%')->get();
    $data['menu_laporan'] = Menu::where('group', 'LIKE', 'laporan%')->get();

    foreach($menu_master as $item) {
      $data['menu_master'][$item->group][] = $item;
    }
    foreach($menu_transaksi as $item) {
      $data['menu_transaksi'][$item->group][] = $item;
    }

    return view('admin.pages.menus.master.pengguna.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token', 'menu_id');
    $this->validate($request, [
      'username' => 'required|max:20',
      'password' => 'required',
      'role' => 'required',
    ]);
    $req['password'] = \Hash::make($req['password']);
    
    $pengguna = User::create($req);

    if (isset($req['menu_id'])) {
      $req = $request->only('menu_id');
      $menu = Menu::all();
      foreach($menu as $item) {
        $hakAkses = Hakakses::create(array(
          'pengguna_id' => $pengguna->id,
          'menu_id' => $item->id,
          'is_allowed' => in_array($item->id, $req['menu_id']),
        ));
      }
    }
    
    if ($pengguna) {
      return redirect('/master/pengguna')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/pengguna')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    // return $id;
    $data['data'] = User::find($id);
    $hakakses = Hakakses::where('pengguna_id', $id)->where('is_allowed', true)->get();
    $data['hakakses'] = array();
    foreach($hakakses as $item) {
      $data['hakakses'][] = $item->menu_id;
    }

    $menu_master = Menu::where('group', 'LIKE', 'master%')->get();
    $menu_transaksi = Menu::where('group', 'LIKE', 'transaksi%')->get();
    $data['menu_laporan'] = Menu::where('group', 'LIKE', 'laporan%')->get();

    foreach($menu_master as $item) {
      $data['menu_master'][$item->group][] = $item;
    }
    foreach($menu_transaksi as $item) {
      $data['menu_transaksi'][$item->group][] = $item;
    }
    
    return view('admin.pages.menus.master.pengguna.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token', 'menu_id');
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $this->validate($request, [
      'username' => 'required|max:20',
      'role' => 'required',
    ]);
    if ($req['password'] != "") {
      $req['password'] = \Hash::make($req['password']);
    }else {
      unset($req['password']);
    }
    $pengguna = User::where('id', $id)->update($req);

    if (isset($req['menu_id'])) {
      $req = $request->only('menu_id');
      $menu = Menu::all();
      Hakakses::where('pengguna_id', $id)->delete();
      foreach($menu as $item) {
        $hakAkses = Hakakses::create(array(
          'pengguna_id' => $id,
          'menu_id' => $item->id,
          'is_allowed' => in_array($item->id, $req['menu_id']),
        ));
      }
    }
    
    if ($pengguna) {
      return redirect('/master/pengguna')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/pengguna')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function delete($id)
  {
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $pengguna = User::find($id);
    $pengguna->delete();
    
    if ($pengguna) {
      return redirect('/master/pengguna')->with([
        'success' => true,
        'message' => 'Berhasil menghapus data',
      ]);
    }

    return redirect('/master/pengguna')->with([
      'success' => false,
      'message' => 'Gagal menghapus data',
    ]);
  }
}
