<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use Response;

class DepartmentController extends Controller
{
    public function index()
    {
        $data['data'] = Department::orderBy('id', 'desc')->get();

        return view('admin.pages.menus.master.department.table', $data);
    }

    public function create()
    {
        return view('admin.pages.menus.master.department.entry');
    }

    public function StoreNew(Request $request)
    {
        $req = $request->except('_token');
        $this->validate($request, [
            'name' => 'required|unique:department|max:30',
        ]);

        $req['name'] = strtoupper($req['name']);

        $department = Department::create($req);

        if ($department) {
            return redirect('/master/department')->with([
                'success' => true,
                'message' => 'Berhasil menyimpan data',
            ]);
        }

        return redirect('/master/department')->with([
            'success' => false,
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function edit($id)
    {
        $data['data'] = Department::find($id);
        return view('admin.pages.menus.master.department.edit', $data);
    }

    public function update($id, Request $request)
    {
        $req = $request->except('_token');
        $this->validate($request, [
            'name' => 'required|max:30',
        ]);

        $req['name'] = strtoupper($req['name']);

        $department = Department::where('id', $id)->update($req);

        if ($department) {
            return redirect('/master/department')->with([
                'success' => true,
                'message' => 'Berhasil menyimpan data',
            ]);
        }

        return redirect('/master/department')->with([
            'success' => false,
            'message' => 'Gagal menyimpan data',
        ]);
    }

    public function delete($id)
    {
        $department = Department::find($id);
        $department->delete();

        if ($department) {
            return redirect('/master/department')->with([
                'success' => true,
                'message' => 'Berhasil menghapus data',
            ]);
        }

        return redirect('/master/department')->with([
            'success' => false,
            'message' => 'Gagal menghapus data',
        ]);
    }
}
