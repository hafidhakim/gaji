<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Penjualan;
use App\Models\Pembayaran;
use App\Models\Pengiriman;

class DashboardController extends Controller
{
    public function index()
    {
        // $data['produk_total'] = Produk::count();
        // $data['penjualan_total'] = Penjualan::count();
        // $data['pembayaran_total'] = Pembayaran::count();
        // $data['pengiriman_total'] = Pengiriman::count();

        return view('admin.pages.dashboard.index');
    }
}
