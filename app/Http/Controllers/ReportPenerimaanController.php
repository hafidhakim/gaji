<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penerimaan;
use App\Models\PenerimaanDetail;
use Response;

class ReportPenerimaanController extends Controller
{
  public function index(Request $request)
  {
    $req = $request->all();
    $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
    if (isset($req['date_range'])) {
      $data['date_range'] = $req['date_range'];
    }
    $date_array = explode(" - ", $data['date_range']);
    $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
    $data['date_start'] = $date_array[0];
    $data['date_end'] = $date_array[1];

    $data['data'] = Penerimaan::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pembelian')->orderBy('id', 'desc')->get();

    return view('admin.pages.menus.report.penerimaan.table', $data);
  }

  public function print(Request $request)
  {
    $req = $request->all();
    $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
    if (isset($req['date_range'])) {
      $data['date_range'] = $req['date_range'];
    }
    $date_array = explode(" - ", $data['date_range']);
    $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
    $data['date_start'] = $date_array[0];
    $data['date_end'] = $date_array[1];

    $data['penerimaan'] = Penerimaan::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pembelian')->orderBy('id', 'desc')->get();

    $pdf = \PDF::loadView('admin.pages.menus.report.penerimaan.print', $data);
    return $pdf->stream('report_penerimaan_'.$data['date_start'].'_'.$data['date_end'].'.pdf');
  }
}
