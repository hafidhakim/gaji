<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengiriman;
use App\Models\PengirimanDetail;
use App\Models\Pembayaran;
use App\Models\Penjualan;
use Response;

class PengirimanController extends Controller
{
  public function home()
  {
    return view ('admin.pages.menus.transaksi.home');
  }

  public function index()
  {
    $data['data'] = Pengiriman::orderBy('id', 'desc')->get();

    return view('admin.pages.menus.transaksi.pengiriman.table', $data);
  }

  public function create(Request $request)
  {
    $data['penjualan'] = Penjualan::doesnthave('pengiriman_detail')->orderBy('id', 'desc')->get();

    return view ('admin.pages.menus.transaksi.pengiriman.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'date' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    \DB::beginTransaction();

		try {
      $penjualan_idArray = $req['penjualan_id'];
      $index = 0;
      $price_total = 0;
      $pengiriman_detail = array();
      foreach($penjualan_idArray as $penjualanID) {
        $penjualan = Penjualan::find($penjualanID);
  
        if ($penjualan) {
          $pengiriman_detail[] = array(
            'penjualan_id' => $penjualan->id,
            'is_delivered' => FALSE,
          );
  
          $index++;
        }
      }

      if (count($pengiriman_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Penjualan harus diisi minimal 1 item',
        ])->withInput();
      }

      $code = Pengiriman::orderBy('code', 'desc')->first();
      if (!$code) {
        $req['code'] = "KIR1";
      }else {
        $code = $code->code;
        $code = str_replace("KIR", "", $code);
        $req['code'] = "KIR" . ($code + 1);
      }
      $req['pengguna_id'] = auth()->user()->id;
      $req['status'] = 'Proses';
      $pengiriman = Pengiriman::create($req);

      foreach($pengiriman_detail as $item) {
        $item['pengiriman_id'] = $pengiriman->id;
        PengirimanDetail::create($item);
      }

			\DB::commit();
			// all good
  
      if ($pengiriman) {
        return redirect('/transaksi/pengiriman/'.$pengiriman->id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/pengiriman')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function show($id)
  {
    $data['pengiriman'] = Pengiriman::with('detail')->find($id);
    $data['penjualan'] = Penjualan::orderBy('id', 'desc')->get();

    return view ('admin.pages.menus.transaksi.pengiriman.detail', $data);
  }

  public function terkirim($pengirimanID, Request $request)
  {
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'penjualan_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    \DB::beginTransaction();

		try {
      $pengirimanDetail = PengirimanDetail::where('pengiriman_id', $pengirimanID)->where('penjualan_id', $req['penjualan_id'])->first();
      $pengirimanDetail->is_delivered = 1;
      $pengirimanDetail->save();

      $checkPengiriman = PengirimanDetail::where('pengiriman_id', $pengirimanID)->where('is_delivered', 0)->count();

      if ($checkPengiriman <= 0) {
        $pengiriman = Pengiriman::find($pengirimanID);
        $pengiriman->status = 'Selesai';
        $pengiriman->save();
      }

			\DB::commit();
			// all good
  
      return 'success';
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return 'failed';
		}
  }

  public function update($id, Request $request)
  {
    $req = $request->only('date', 'suplier_id');
    $reqDetail = $request->only('produk_id','qty','produk_price');
    $validator = $this->validate($request, [
      'date' => 'required',
      'suplier_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    \DB::beginTransaction();

		try {
      $produk_idArray = $reqDetail['produk_id'];
      $qtyArray = $reqDetail['qty'];
      $produk_priceArray = $reqDetail['produk_price'];
      $index = 0;
      $price_total = 0;
      $pengiriman_detail = array();
      foreach($produk_idArray as $produkID) {
        $produk = Produk::find($produkID);
  
        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $qty = $qtyArray[$index];
          $produk_price = $produk_priceArray[$index];
          $price_subtotal = $qty * $produk_price;
  
          $pengiriman_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );
  
          // SUM for total
          $price_total += $price_subtotal;
  
          $index++;
        }
      }

      if (count($pengiriman_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Produk harus diisi minimal 1 item',
        ])->withInput();
      }

      $req['price_total'] = $price_total;
      $pengiriman = Pengiriman::where('id', $id)->update($req);

      PengirimanDetail::where('pengiriman_id', $id)->delete();
      foreach($pengiriman_detail as $item) {
        $item['pengiriman_id'] = $id;
        PengirimanDetail::create($item);
      }

			\DB::commit();
			// all good
  
      if ($pengiriman) {
        return redirect('/transaksi/pengiriman/'.$id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/pengiriman')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function print($id)
  {
    $data['pengiriman'] = Pengiriman::with('detail')->find($id);

    $pdf = \PDF::loadView('admin.pages.menus.transaksi.pengiriman.print', $data);
    return $pdf->stream('pengiriman_'.$id.'.pdf');
  }
}
