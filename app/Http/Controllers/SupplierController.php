<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;
use Response;

class SupplierController extends Controller
{
  public function index()
  {
    $data['data'] = Supplier::orderBy('id', 'desc')->get();

    return view('admin.pages.menus.master.supplier.table', $data);
  }

  public function create()
  {
    $data['newcode'] = Supplier::orderBy('id', 'desc')->first();
    if (!$data['newcode']) {
      $data['newcode'] = "SUP1";
    }else {
      $data['newcode'] = "SUP" . ($data['newcode']->id + 1);
    }
    return view('admin.pages.menus.master.supplier.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $this->validate($request, [
      'name' => 'required|max:30',
      'phone' => 'required|max:20',
      'address' => 'required',
    ]);
    $supplier = Supplier::create($req);

    if ($supplier) {
      return redirect('/master/supplier')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/supplier')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $data['data'] = Supplier::find($id);

    return view('admin.pages.menus.master.supplier.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token','code');
    $this->validate($request, [
      'name' => 'required|max:30',
      'phone' => 'required|max:20',
      'address' => 'required',
    ]);
    $supplier = Supplier::where('id', $id)->update($req);

    if ($supplier) {
      return redirect('/master/supplier')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/supplier')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function delete($id)
  {
    $supplier = Supplier::find($id);
    $supplier->delete();

    if ($supplier) {
      return redirect('/master/supplier')->with([
        'success' => true,
        'message' => 'Berhasil menghapus data',
      ]);
    }

    return redirect('/master/supplier')->with([
      'success' => false,
      'message' => 'Gagal menghapus data',
    ]);
  }
}
