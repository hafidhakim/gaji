<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Komisi;
use App\Models\Pelanggan;
use Response;

class KomisiController extends Controller
{
  public function index(Request $request)
  {
    $req = $request->all();
    $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
    if (isset($req['date_range'])) {
      $data['date_range'] = $req['date_range'];
    }
    $date_array = explode(" - ", $data['date_range']);
    $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
    $data['date_start'] = $date_array[0];
    $data['date_end'] = $date_array[1];

    $data['data'] = Komisi::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pelanggan')->orderBy('id', 'desc')->get();
    // return $data;
    return view('admin.pages.menus.transaksi.komisi.table', $data);
  }

  public function create(Request $request)
  {
    $data['pelanggan'] = Pelanggan::where('department_id', '=', 4)->orderBy('id', 'desc')->get();

    return view ('admin.pages.menus.transaksi.komisi.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    // $total = 0;
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'pegawai_id' => 'required',
      'omzet_tunai' => 'required|numeric',
      'pct_tunai' => 'required|numeric',
      'omzet_tunai' => 'required|numeric',
      'pct_kredit' => 'required|numeric',
    ]);
    if ($validator and $validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $req['komisi_tunai'] = $req['omzet_tunai']*$req['pct_tunai']/100;
    $req['komisi_kredit'] = $req['omzet_kredit']*$req['pct_kredit']/100;
    $req['grand_total'] = $req['komisi_tunai']+$req['komisi_kredit']+$req['bonus']+$req['komisi_lain']
    -$req['bon_uang']-$req['potongan_opname']-$req['potongan_lain'];

    $komisi = Komisi::create($req);

    if ($komisi) {
      return redirect('/transaksi/komisi')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/produk')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function edit($id)
  {
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $data['komisi'] = Komisi::find($id);
    // $data['pelanggan'] = Pelanggan::orderBy('id', 'desc')->get();
    $data['pelanggan'] = Pelanggan::where('department_id', '=', 4)->orderBy('id', 'desc')->get();
    // return $data;

    return view ('admin.pages.menus.transaksi.komisi.edit', $data);
  }

  public function update($id, Request $request)
  {
    $req = $request->except('_token');
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $validator = $this->validate($request, [
        'date' => 'required',
        'pegawai_id' => 'required',
        'omzet_tunai' => 'required|numeric',
        'pct_tunai' => 'required|numeric',
        'omzet_tunai' => 'required|numeric',
        'pct_kredit' => 'required|numeric',
    ]);
    if ($validator and $validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
    }

    $req['komisi_tunai'] = $req['omzet_tunai']*$req['pct_tunai']/100;
    $req['komisi_kredit'] = $req['omzet_kredit']*$req['pct_kredit']/100;
    $req['grand_total'] = $req['komisi_tunai']+$req['komisi_kredit']+$req['bonus']+$req['komisi_lain']
    -$req['bon_uang']-$req['potongan_opname']-$req['potongan_lain'];

    $komisi = Komisi::where('id', $id)->update($req);

    if ($komisi) {
      return redirect('/transaksi/komisi')->with([
        'success' => true,
        'message' => 'Berhasil menyimpan data',
      ]);
    }

    return redirect('/master/produk')->with([
      'success' => false,
      'message' => 'Gagal menyimpan data',
    ]);
  }

  public function show($id)
  {
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $data['komisi'] = Komisi::with('pelanggan')->find($id);
    // return $data;

    return view ('admin.pages.menus.transaksi.komisi.detail', $data);
  }

  public function print($id)
  {
    $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
    $id = $encrypter->decrypt($id);
    $data['komisi'] = Komisi::with('pelanggan.department')->find($id);
//    return $data;

    $pdf = \PDF::loadView('admin.pages.menus.transaksi.komisi.print', $data)->setPaper('a5', 'landscape');
    return $pdf->stream('komisi_'.$id.'.pdf');
  }

  public function prints(Request $request)
  {
    $req = $request->all();
    $data['date_range'] = date("Y/m/d") . ' - ' . date("Y/m/d");
    if (isset($req['date_range'])) {
      $data['date_range'] = $req['date_range'];
    }
    $date_array = explode(" - ", $data['date_range']);
    $date_array = preg_replace('/[\W\s\/]+/', '-', $date_array);
    $data['date_start'] = $date_array[0];
    $data['date_end'] = $date_array[1];

    $data['komisi'] = Komisi::whereBetween('date', [$data['date_start'], $data['date_end']])->with('pelanggan.department')->get();
    // return $data;

    $pdf = \PDF::loadView('admin.pages.menus.transaksi.komisi.prints', $data)->setPaper('a5', 'landscape');
    return $pdf->stream('komisi_'.$data['date_start'].'_'.$data['date_end'].'.pdf');
  }
}
