<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penjualan;
use App\Models\PenjualanDetail;
use App\Models\Pembayaran;
use App\Models\Pelanggan;
use App\Models\Produk;
use Response;

class PenjualanController extends Controller
{
  public function home()
  {
    return view ('admin.pages.menus.transaksi.home');
  }

  public function index()
  {
    $data['data'] = Penjualan::with('pelanggan')->orderBy('id', 'desc')->get();

    return view('admin.pages.menus.transaksi.penggajian.table', $data);
  }

  public function itemHTML(Request $request)
  {
    $req = $request->all();

    $data['data'] = Penjualan::orderBy('id', 'desc')->get();
    $data['countrow'] = $req['countrow'];
    $data['status_pengiriman'] = $req['status_pengiriman'];

    return view('admin.pages.menus.transaksi.penjualan.item', $data);
  }

  public function create(Request $request)
  {
    $produk_idArray = $request->old('produk_id');
    $qtyArray = $request->old('qty');
    $index = 0;
    $price_total = 0;
    $penjualan_detail = array();
    if ($produk_idArray) {
      foreach($produk_idArray as $produkID) {
        $produk = Produk::with('kategori','satuan')->find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $kategori = $produk->kategori ? $produk->kategori->name : '';
          $stok = $produk->stock;
          $satuan = $produk->satuan ? $produk->satuan->name : '';
          $qty = $qtyArray[$index];
          $produk_price = $produk->price_sell;
          $price_subtotal = $qty * $produk_price;

          $penjualan_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'kategori' => $kategori,
            'stok' => $stok,
            'satuan' => $satuan,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          $index++;
        }
      }
    }

    $data['pelanggan'] = Pelanggan::orderBy('id', 'desc')->get();
    $data['produk'] = Produk::orderBy('id', 'desc')->get();
    $data['old_penjualan_detail'] = $penjualan_detail;
    $data['old_price_total'] = $price_total;

    return view ('admin.pages.menus.transaksi.penggajian.entry', $data);
  }

  public function StoreNew(Request $request)
  {
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'date' => 'required',
      'pelanggan_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    if ($req['amount_pay'] <= 0) {
      return redirect()->back()->with([
        'success' => false,
        'message' => 'Tidak dapat melakukan pembayaran dengan data minus',
      ]);
    }

    \DB::beginTransaction();

		try {
      $produk_idArray = $req['produk_id'];
      $qtyArray = $req['qty'];
      $index = 0;
      $price_total = 0;
      $penjualan_detail = array();
      foreach($produk_idArray as $produkID) {
        $produk = Produk::find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $qty = $qtyArray[$index];
          $produk_price = $produk->price_sell;
          $price_subtotal = $qty * $produk_price;

          $penjualan_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          // update stok produk
          $produk->stock = $produk->stock - $qty;
          $produk->save();

          $index++;
        }
      }

      if (count($penjualan_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Produk harus diisi minimal 1 item',
        ])->withInput();
      }

      $code = Penjualan::orderBy('code', 'desc')->first();
      if (!$code) {
        $req['code'] = "JUAL1";
      }else {
        $code = $code->code;
        $code = str_replace("JUAL", "", $code);
        $req['code'] = "JUAL" . ($code + 1);
      }
      $req['pengguna_id'] = auth()->user()->id;
      $req['price_total'] = $price_total;
      $penjualan = Penjualan::create($req);

      foreach($penjualan_detail as $item) {
        $item['penjualan_id'] = $penjualan->id;
        PenjualanDetail::create($item);
      }

      // Proses pembayaran
      $code = Pembayaran::orderBy('code', 'desc')->first();
      if (!$code) {
        $code = "BAY1";
      }else {
        $code = $code->code;
        $code = str_replace("BAY", "", $code);
        $code = "BAY" . ($code + 1);
      }
      $reqPembayaran = array(
        'penjualan_id' => $penjualan->id,
        'code' => $code,
        'date' => $req['date'],
        'amount_pay' => $req['amount_pay'],
        'amount_left' => $penjualan->price_total - $req['amount_pay'],
        'amount_total' => $penjualan->price_total,
        'type' => 'penjualan',
        'is_paidoff' => TRUE,
      );
      $pembayaran = Pembayaran::create($reqPembayaran);

			\DB::commit();
			// all good

      if ($pembayaran) {
        return redirect('/transaksi/penjualan/'.$penjualan->id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/penjualan')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function show($id)
  {
    $data['penjualan'] = Penjualan::with('detail','pembayaran')->find($id);
    $data['pelanggan'] = Pelanggan::orderBy('id', 'desc')->get();
    $data['produk'] = Produk::orderBy('id', 'desc')->get();

    return view ('admin.pages.menus.transaksi.penjualan.detail', $data);
  }

  public function bayar($id)
  {
    $data['penjualan'] = Penjualan::find($id);

    return view ('admin.pages.menus.transaksi.penjualan.bayar', $data);
  }

  public function bayarSave($id, Request $request)
  {
    $req = $request->except('_token');
    $validator = $this->validate($request, [
      'date' => 'required',
      'amount_pay' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    if ($req['amount_pay'] <= 0) {
      return redirect()->back()->with([
        'success' => false,
        'message' => 'Tidak dapat melakukan pembayaran dengan data minus',
      ]);
    }

    \DB::beginTransaction();

		try {
      $penjualan = Penjualan::find($id);

      // Proses pembayaran
      $code = Pembayaran::orderBy('code', 'desc')->first();
      if (!$code) {
        $code = "BAY1";
      }else {
        $code = $code->code;
        $code = str_replace("BAY", "", $code);
        $code = "BAY" . ($code + 1);
      }
      $reqPembayaran = array(
        'penjualan_id' => $penjualan->id,
        'code' => $code,
        'date' => $req['date'],
        'amount_pay' => $req['amount_pay'],
        'amount_left' => $penjualan->sisa_bayar - $req['amount_pay'],
        'amount_total' => $penjualan->price_total,
        'type' => 'penjualan',
        'is_paidoff' => TRUE,
      );
      $pembayaran = Pembayaran::create($reqPembayaran);

			\DB::commit();
			// all good

      if ($pembayaran) {
        return redirect('/transaksi/penjualan/'.$penjualan->id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/penjualan')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function update($id, Request $request)
  {
    $req = $request->only('date', 'pelanggan_id');
    $reqDetail = $request->only('produk_id','qty','produk_price');
    $validator = $this->validate($request, [
      'date' => 'required',
      'pelanggan_id' => 'required',
    ]);
    if ($validator and $validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    \DB::beginTransaction();

		try {
      $produk_idArray = $reqDetail['produk_id'];
      $qtyArray = $reqDetail['qty'];
      $index = 0;
      $price_total = 0;
      $penjualan_detail = array();
      foreach($produk_idArray as $produkID) {
        $produk = Produk::find($produkID);

        if ($produk) {
          $produk_id = $produk->id;
          $produk_name = $produk->name;
          $qty = $qtyArray[$index];
          $produk_price = $produk->price_sell;
          $price_subtotal = $qty * $produk_price;

          $penjualan_detail[] = array(
            'produk_id' => $produk_id,
            'produk_name' => $produk_name,
            'qty' => $qty,
            'produk_price' => $produk_price,
            'price_subtotal' => $price_subtotal,
          );

          // SUM for total
          $price_total += $price_subtotal;

          $index++;
        }
      }

      if (count($penjualan_detail) <= 0) {
        return redirect()->back()->with([
          'success' => false,
          'message' => 'Produk harus diisi minimal 1 item',
        ])->withInput();
      }

      $req['price_total'] = $price_total;
      $penjualan = Penjualan::where('id', $id)->update($req);

      PenjualanDetail::where('penjualan_id', $id)->delete();
      foreach($penjualan_detail as $item) {
        $item['penjualan_id'] = $id;
        PenjualanDetail::create($item);
      }

			\DB::commit();
			// all good

      if ($penjualan) {
        return redirect('/transaksi/penjualan/'.$id)->with([
          'success' => true,
          'message' => 'Berhasil menyimpan data',
        ]);
      }
		} catch (\Exception $e) {
			\DB::rollback();
			// something went wrong
      return redirect('/transaksi/penjualan')->with([
        'success' => false,
        'message' => 'Gagal menyimpan data .' . $e->getMessage(),
      ]);
		}
  }

  public function print($id)
  {
    $data['penjualan'] = Penjualan::with('detail','pelanggan')->find($id);

    $pdf = \PDF::loadView('admin.pages.menus.transaksi.penjualan.print', $data);
    return $pdf->stream('penjualan_'.$id.'.pdf');
  }
}
