<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hakakses extends Model
{
    protected $table = 'hakakses';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'pengguna_id',
        'menu_id',
        'is_allowed',
    ];

    protected $dates = ['deleted_at'];
}
