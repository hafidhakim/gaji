<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 25 Aug 2019 07:50:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payroll
 *
 * @property int $id
 * @property int $pegawai_id
 * @property \Carbon\Carbon $date
 * @property int $gaji_pokok
 * @property int $tunjangan_jabatan
 * @property int $uang_makan
 * @property int $uang_premi
 * @property int $gaji_lain
 * @property int $bon_barang
 * @property int $bon_uang
 * @property int $terlambat
 * @property int $potongan_lain
 * @property int $total
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Payroll extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'pegawai_id' => 'int',
		'gaji_pokok' => 'int',
		'tunjangan_jabatan' => 'int',
		'uang_makan' => 'int',
		'uang_premi' => 'int',
		'gaji_lain' => 'int',
		'bon_barang' => 'int',
		'bon_uang' => 'int',
		'terlambat' => 'int',
		'potongan_lain' => 'int',
		'total' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'pegawai_id',
		'date',
		'gaji_pokok',
		'tunjangan_jabatan',
		'uang_makan',
		'uang_premi',
		'gaji_lain',
		'bon_barang',
		'bon_uang',
		'terlambat',
		'potongan_lain',
		'total'
	];

	public function pelanggan() {
			return $this->belongsTo(Pelanggan::class, 'pegawai_id')->withTrashed();
	}

	protected $appends = ['eid'];

	public function getEidAttribute() {
		$encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
		return $encrypter->encrypt($this->id);
	}
}
