<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produk extends Model
{
    use SoftDeletes;
    protected $table = 'produk';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'kategori_id',
        'satuan_id',
        'code',
        'name',
        'price_buy',
        'price_sell',
        'stock',
    ];

    protected $dates = ['deleted_at'];

    public function kategori() {
        return $this->belongsTo(Kategori::class)->withTrashed();
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class)->withTrashed();
    }
}
