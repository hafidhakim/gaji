<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pembayaran extends Model
{
    use SoftDeletes;
    protected $table = 'pembayaran';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'penjualan_id',
        'pembelian_id',
        'code',
        'date',
        'amount_pay',
        'amount_left',
        'amount_total',
        'type',
        'is_paidoff',
    ];

    protected $dates = ['deleted_at'];

    public function penjualan() {
        return $this->belongsTo(Penjualan::class, 'penjualan_id')->withTrashed();
    }

    public function pembelian() {
        return $this->belongsTo(Pembelian::class, 'pembelian_id')->withTrashed();
    }
}
