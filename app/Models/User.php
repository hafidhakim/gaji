<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $table = 'pengguna';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['eid'];

    public function department()
    {
        return $this->belongsTo(\App\Models\Department::class);
    }

    public function getEidAttribute() {
        // return $this->id;
		$encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
		return $encrypter->encrypt($this->id);
	}

}
