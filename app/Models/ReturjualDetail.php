<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturjualDetail extends Model
{
    protected $table = 'returjual_detail';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'returjual_id',
        'produk_id',
        'produk_name',
        'produk_price',
        'qty',
        'price_subtotal',
    ];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id')->with('kategori','satuan')->withTrashed();
    }
}
