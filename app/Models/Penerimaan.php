<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Penerimaan extends Model
{
    use SoftDeletes;
    protected $table = 'penerimaan';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'pengguna_id',
        'pembelian_id',
        'code',
        'date',
        'price_total',
    ];

    protected $dates = ['deleted_at'];

    public function pembelian() {
        return $this->belongsTo(Pembelian::class, 'pembelian_id')->with('supplier')->withTrashed();
    }

    public function detail() {
        return $this->hasMany(PenerimaanDetail::class, 'penerimaan_id');
    }
}
