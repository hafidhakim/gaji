<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 19 Nov 2019 16:10:09 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Pelanggan
 * 
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $department_id
 * @property int $gaji_pokok
 * @property int $tunjangan_jabatan
 * @property string $phone
 * @property string $address
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 *
 * @package App\Models
 */
// use Illuminate\Contracts\Encryption\Encrypter;

class Pelanggan extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pelanggan';

	protected $casts = [
		'department_id' => 'int',
		'gaji_pokok' => 'int',
		'tunjangan_jabatan' => 'int'
	];

	protected $fillable = [
		'code',
		'name',
		'department_id',
		'gaji_pokok',
		'tunjangan_jabatan',
		'phone',
		'address'
	];

	protected $appends = ['eid'];

	
	public function department() {
		return $this->belongsTo(Department::class, 'department_id')->withTrashed();
	}

	public function getEidAttribute() {
		$encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
		return $encrypter->encrypt($this->id);
	}
}
