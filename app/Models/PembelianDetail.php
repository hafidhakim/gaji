<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PembelianDetail extends Model
{
    protected $table = 'pembelian_detail';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'pembelian_id',
        'produk_id',
        'produk_name',
        'produk_price',
        'qty',
        'price_subtotal',
    ];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id')->with('kategori','satuan')->withTrashed();
    }
}
