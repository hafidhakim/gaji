<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Absensi extends Model
{
    use SoftDeletes;
    protected $table = 'absensi';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'pengguna_id',
        'date',
        'time_start',
        'time_end',
        'is_approved',
    ];

    protected $dates = ['deleted_at'];

    public function pengguna() {
        return $this->belongsTo(User::class, 'pengguna_id')->withTrashed();
    }
}
