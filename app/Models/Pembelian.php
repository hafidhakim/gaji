<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pembelian extends Model
{
    use SoftDeletes;
    protected $table = 'pembelian';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'suplier_id',
        'pengguna_id',
        'code',
        'date',
        'price_total',
    ];

    protected $dates = ['deleted_at'];

    protected $appends = ['sudah_bayar', 'sisa_bayar', 'status_tagihan'];

    public function getSudahBayarAttribute($value)
    {
      $attributes = $this->getAttributes();
      
      $sudahBayar = Pembayaran::where('pembelian_id', $attributes['id'])->sum('amount_pay');

      if (count($sudahBayar) > 0) {
        return $sudahBayar;
      }

      return 0;
    }

    public function getSisaBayarAttribute($value)
    {
      $attributes = $this->getAttributes();
      
      $sudahBayar = Pembayaran::where('pembelian_id', $attributes['id'])->sum('amount_pay');

      return $attributes['price_total'] - $sudahBayar;
    }

    public function getStatusTagihanAttribute($value)
    {
      $attributes = $this->getAttributes();
      
      $sudahBayar = Pembayaran::where('pembelian_id', $attributes['id'])->sum('amount_pay');

      if ($attributes['price_total'] == $sudahBayar) {
        return "Lunas";
      }else if ($sudahBayar > 0) {
        return "Angsur";
      }

      return "Belum Bayar";
    }

    public function supplier() {
        return $this->belongsTo(Supplier::class, 'suplier_id')->withTrashed();
    }

    public function detail() {
        return $this->hasMany(PembelianDetail::class, 'pembelian_id');
    }

    public function pembayaran() {
        return $this->hasMany(Pembayaran::class, 'pembelian_id');
    }

    public function penerimaan() {
        return $this->hasOne(Penerimaan::class, 'pembelian_id');
    }
}
