<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 16 Nov 2019 13:00:20 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Department
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Department extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'department';

	protected $fillable = [
		'name'
	];

    public function users()
    {
        return $this->hasMany(\App\Models\User::class);
    }
}
