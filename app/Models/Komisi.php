<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 03 Feb 2020 22:48:17 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Komisi
 * 
 * @property int $id
 * @property int $pegawai_id
 * @property \Carbon\Carbon $date
 * @property int $omzet_tunai
 * @property int $pct_tunai
 * @property int $komisi_tunai
 * @property int $omzet_kredit
 * @property int $pct_kredit
 * @property int $komisi_kredit
 * @property int $bonus
 * @property int $komisi_lain
 * @property int $bon_uang
 * @property int $potongan_opname
 * @property int $potongan_lain
 * @property int $grand_total
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Komisi extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'komisi';

	protected $casts = [
		'pegawai_id' => 'int',
		'omzet_tunai' => 'int',
		'pct_tunai' => 'int',
		'komisi_tunai' => 'int',
		'omzet_kredit' => 'int',
		'pct_kredit' => 'int',
		'komisi_kredit' => 'int',
		'bonus' => 'int',
		'komisi_lain' => 'int',
		'bon_uang' => 'int',
		'potongan_opname' => 'int',
		'potongan_lain' => 'int',
		'grand_total' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'pegawai_id',
		'date',
		'omzet_tunai',
		'pct_tunai',
		'komisi_tunai',
		'omzet_kredit',
		'pct_kredit',
		'komisi_kredit',
		'bonus',
		'komisi_lain',
		'bon_uang',
		'potongan_opname',
		'potongan_lain',
		'grand_total'
	];

    public function pelanggan() {
        return $this->belongsTo(Pelanggan::class, 'pegawai_id')->withTrashed();
    }

    protected $appends = ['eid'];

    public function getEidAttribute() {
        $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        return $encrypter->encrypt($this->id);
    }
}
