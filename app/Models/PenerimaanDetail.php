<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenerimaanDetail extends Model
{
    protected $table = 'penerimaan_detail';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'penerimaan_id',
        'produk_id',
        'produk_name',
        'produk_price',
        'qty',
        'price_subtotal',
    ];

    public function produk() {
        return $this->belongsTo(Produk::class, 'produk_id')->with('kategori','satuan')->withTrashed();
    }
}
