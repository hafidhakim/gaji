<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengiriman extends Model
{
    use SoftDeletes;
    protected $table = 'pengiriman';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'pengguna_id',
        'code',
        'date',
        'status',
    ];

    protected $dates = ['deleted_at'];

    public function detail() {
        return $this->hasMany(PengirimanDetail::class, 'pengiriman_id');
    }
}
