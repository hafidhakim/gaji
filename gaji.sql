-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: gajis
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `absensi`
--

DROP TABLE IF EXISTS `absensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `absensi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengguna_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `absensi`
--

LOCK TABLES `absensi` WRITE;
/*!40000 ALTER TABLE `absensi` DISABLE KEYS */;
/*!40000 ALTER TABLE `absensi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hakakses`
--

DROP TABLE IF EXISTS `hakakses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hakakses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pengguna_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `is_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hakakses`
--

LOCK TABLES `hakakses` WRITE;
/*!40000 ALTER TABLE `hakakses` DISABLE KEYS */;
INSERT INTO `hakakses` VALUES (1,2,1,0,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(2,2,2,0,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(3,2,3,0,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(4,2,4,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(5,2,5,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(6,2,6,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(7,2,7,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(8,2,8,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(9,2,9,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(10,2,10,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(11,2,11,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(12,2,12,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(13,2,13,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(14,2,14,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(15,2,15,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(16,2,16,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(17,2,17,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(18,2,18,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(19,2,19,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(20,2,20,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(21,2,21,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(22,2,22,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(23,2,23,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(24,2,24,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(25,2,25,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(26,2,26,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(27,2,27,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(28,2,28,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(29,2,29,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(30,2,30,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(31,2,31,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(32,2,32,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(33,2,33,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(34,2,34,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(35,2,35,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(36,2,36,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(37,2,37,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(38,2,38,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(39,2,39,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(40,2,40,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(41,2,41,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(42,2,42,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(43,2,43,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(44,2,44,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(45,2,45,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(46,2,46,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(47,2,47,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(48,2,48,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(49,2,49,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(50,3,1,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(51,3,2,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(52,3,3,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(53,3,4,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(54,3,5,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(55,3,6,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(56,3,7,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(57,3,8,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(58,3,9,0,'2019-08-21 22:25:12','2019-08-21 22:25:12'),(59,3,10,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(60,3,11,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(61,3,12,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(62,3,13,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(63,3,14,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(64,3,15,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(65,3,16,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(66,3,17,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(67,3,18,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(68,3,19,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(69,3,20,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(70,3,21,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(71,3,22,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(72,3,23,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(73,3,24,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(74,3,25,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(75,3,26,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(76,3,27,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(77,3,28,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(78,3,29,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(79,3,30,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(80,3,31,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(81,3,32,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(82,3,33,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(83,3,34,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(84,3,35,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(85,3,36,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(86,3,37,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(87,3,38,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(88,3,39,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(89,3,40,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(90,3,41,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(91,3,42,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(92,3,43,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(93,3,44,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(94,3,45,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(95,3,46,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(96,3,47,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(97,3,48,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(98,3,49,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(99,4,1,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(100,4,2,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(101,4,3,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(102,4,4,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(103,4,5,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(104,4,6,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(105,4,7,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(106,4,8,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(107,4,9,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(108,4,10,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(109,4,11,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(110,4,12,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(111,4,13,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(112,4,14,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(113,4,15,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(114,4,16,0,'2019-08-21 22:25:13','2019-08-21 22:25:13'),(115,4,17,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(116,4,18,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(117,4,19,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(118,4,20,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(119,4,21,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(120,4,22,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(121,4,23,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(122,4,24,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(123,4,25,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(124,4,26,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(125,4,27,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(126,4,28,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(127,4,29,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(128,4,30,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(129,4,31,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(130,4,32,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(131,4,33,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(132,4,34,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(133,4,35,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(134,4,36,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(135,4,37,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(136,4,38,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(137,4,39,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(138,4,40,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(139,4,41,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(140,4,42,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(141,4,43,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(142,4,44,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(143,4,45,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(144,4,46,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(145,4,47,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(146,4,48,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(147,4,49,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(148,5,1,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(149,5,2,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(150,5,3,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(151,5,4,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(152,5,5,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(153,5,6,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(154,5,7,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(155,5,8,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(156,5,9,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(157,5,10,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(158,5,11,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(159,5,12,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(160,5,13,0,'2019-08-21 22:25:14','2019-08-21 22:25:14'),(161,5,14,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(162,5,15,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(163,5,16,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(164,5,17,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(165,5,18,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(166,5,19,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(167,5,20,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(168,5,21,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(169,5,22,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(170,5,23,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(171,5,24,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(172,5,25,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(173,5,26,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(174,5,27,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(175,5,28,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(176,5,29,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(177,5,30,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(178,5,31,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(179,5,32,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(180,5,33,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(181,5,34,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(182,5,35,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(183,5,36,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(184,5,37,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(185,5,38,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(186,5,39,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(187,5,40,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(188,5,41,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(189,5,42,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(190,5,43,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(191,5,44,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(192,5,45,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(193,5,46,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(194,5,47,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(195,5,48,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(196,5,49,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(197,6,1,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(198,6,2,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(199,6,3,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(200,6,4,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(201,6,5,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(202,6,6,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(203,6,7,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(204,6,8,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(205,6,9,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(206,6,10,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(207,6,11,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(208,6,12,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(209,6,13,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(210,6,14,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(211,6,15,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(212,6,16,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(213,6,17,0,'2019-08-21 22:25:15','2019-08-21 22:25:15'),(214,6,18,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(215,6,19,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(216,6,20,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(217,6,21,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(218,6,22,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(219,6,23,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(220,6,24,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(221,6,25,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(222,6,26,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(223,6,27,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(224,6,28,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(225,6,29,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(226,6,30,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(227,6,31,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(228,6,32,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(229,6,33,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(230,6,34,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(231,6,35,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(232,6,36,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(233,6,37,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(234,6,38,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(235,6,39,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(236,6,40,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(237,6,41,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(238,6,42,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(239,6,43,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(240,6,44,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(241,6,45,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(242,6,46,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(243,6,47,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(244,6,48,0,'2019-08-21 22:25:16','2019-08-21 22:25:16'),(245,6,49,0,'2019-08-21 22:25:16','2019-08-21 22:25:16');
/*!40000 ALTER TABLE `hakakses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'view-master-pelanggan','Lihat Master Pelanggan','master-pelanggan',NULL,'2019-08-21 22:25:10','2019-08-21 22:25:10'),(2,'create-master-pelanggan','Tambah Master Pelanggan','master-pelanggan',NULL,'2019-08-21 22:25:10','2019-08-21 22:25:10'),(3,'edit-master-pelanggan','Ubah Master Pelanggan','master-pelanggan',NULL,'2019-08-21 22:25:10','2019-08-21 22:25:10'),(4,'delete-master-pelanggan','Hapus Master Pelanggan','master-pelanggan',NULL,'2019-08-21 22:25:10','2019-08-21 22:25:10'),(5,'view-master-supplier','Lihat Master Supplier','master-supplier',NULL,'2019-08-21 22:25:10','2019-08-21 22:25:10'),(6,'create-master-supplier','Tambah Master Supplier','master-supplier',NULL,'2019-08-21 22:25:10','2019-08-21 22:25:10'),(7,'edit-master-supplier','Ubah Master Supplier','master-supplier',NULL,'2019-08-21 22:25:10','2019-08-21 22:25:10'),(8,'delete-master-supplier','Hapus Master Supplier','master-supplier',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(9,'view-master-pengguna','Lihat Master Pengguna','master-pengguna',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(10,'create-master-pengguna','Tambah Master Pengguna','master-pengguna',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(11,'edit-master-pengguna','Ubah Master Pengguna','master-pengguna',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(12,'delete-master-pengguna','Hapus Master Pengguna','master-pengguna',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(13,'view-master-kategori','Lihat Master Kategori','master-kategori',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(14,'create-master-kategori','Tambah Master Kategori','master-kategori',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(15,'edit-master-kategori','Ubah Master Kategori','master-kategori',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(16,'delete-master-kategori','Hapus Master Kategori','master-kategori',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(17,'view-master-satuan','Lihat Master Satuan','master-satuan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(18,'create-master-satuan','Tambah Master Satuan','master-satuan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(19,'edit-master-satuan','Ubah Master Satuan','master-satuan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(20,'delete-master-satuan','Hapus Master Satuan','master-satuan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(21,'view-master-produk','Lihat Master Produk','master-produk',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(22,'create-master-produk','Tambah Master Produk','master-produk',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(23,'edit-master-produk','Ubah Master Produk','master-produk',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(24,'delete-master-produk','Hapus Master Produk','master-produk',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(25,'view-transaksi-pembelian','Lihat Transaksi Pembelian','transaksi-pembelian',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(26,'create-transaksi-pembelian','Tambah Transaksi Pembelian','transaksi-pembelian',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(27,'edit-transaksi-pembelian','Ubah Transaksi Pembelian','transaksi-pembelian',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(28,'delete-transaksi-pembelian','Hapus Transaksi Pembelian','transaksi-pembelian',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(29,'view-transaksi-penjualan','Lihat Transaksi Penjualan','transaksi-penjualan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(30,'create-transaksi-penjualan','Tambah Transaksi Penjualan','transaksi-penjualan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(31,'edit-transaksi-penjualan','Ubah Transaksi Penjualan','transaksi-penjualan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(32,'delete-transaksi-penjualan','Hapus Transaksi Penjualan','transaksi-penjualan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(33,'view-transaksi-returjual','Lihat Transaksi Returjual','transaksi-returjual',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(34,'create-transaksi-returjual','Tambah Transaksi Returjual','transaksi-returjual',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(35,'edit-transaksi-returjual','Ubah Transaksi Returjual','transaksi-returjual',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(36,'delete-transaksi-returjual','Hapus Transaksi Returjual','transaksi-returjual',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(37,'view-transaksi-pengiriman','Lihat Transaksi Pengiriman','transaksi-pengiriman',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(38,'create-transaksi-pengiriman','Tambah Transaksi Pengiriman','transaksi-pengiriman',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(39,'edit-transaksi-pengiriman','Ubah Transaksi Pengiriman','transaksi-pengiriman',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(40,'delete-transaksi-pengiriman','Hapus Transaksi Pengiriman','transaksi-pengiriman',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(41,'view-transaksi-pembayaran','Lihat Transaksi Pembayaran','transaksi-pembayaran',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(42,'create-transaksi-pembayaran','Tambah Transaksi Pembayaran','transaksi-pembayaran',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(43,'edit-transaksi-pembayaran','Ubah Transaksi Pembayaran','transaksi-pembayaran',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(44,'delete-transaksi-pembayaran','Hapus Transaksi Pembayaran','transaksi-pembayaran',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(45,'view-laporan-pembelian','Lihat Laporan Pembelian','laporan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(46,'view-laporan-penjualan','Lihat Laporan Penjualan','laporan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(47,'view-laporan-returjual','Lihat Laporan Returjual','laporan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(48,'view-laporan-pengiriman','Lihat Laporan Pengiriman','laporan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11'),(49,'view-laporan-pembayaran','Lihat Laporan Pembayaran','laporan',NULL,'2019-08-21 22:25:11','2019-08-21 22:25:11');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_pengguna_table',1),(2,'2019_02_17_113552_create_pelanggan_table',1),(3,'2019_02_17_113603_create_menu_table',1),(4,'2019_02_17_113611_create_satuan_table',1),(5,'2019_02_17_113621_create_kategori_table',1),(6,'2019_02_17_113635_create_hakakses_table',1),(7,'2019_02_17_113647_create_suplier_table',1),(8,'2019_02_17_113701_create_produk_table',1),(9,'2019_02_17_113717_create_pembelian_table',1),(10,'2019_02_17_113722_create_pembelian_detail_table',1),(11,'2019_02_17_113801_create_penjualan_table',1),(12,'2019_02_17_113806_create_penjualan_detail_table',1),(13,'2019_02_17_113818_create_returjual_table',1),(14,'2019_02_17_113824_create_returjual_detail_table',1),(15,'2019_02_17_113840_create_pembayaran_table',1),(16,'2019_02_17_113849_create_pengiriman_table',1),(17,'2019_02_17_113856_create_pengiriman_detail_table',1),(18,'2019_02_17_113915_create_absensi_table',1),(19,'2019_05_22_084539_create_penerimaan_table',1),(20,'2019_05_22_084547_create_penerimaan_detail_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payrolls`
--

DROP TABLE IF EXISTS `payrolls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payrolls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `gaji_pokok` int(11) DEFAULT NULL,
  `tunjangan_jabatan` int(11) DEFAULT '0',
  `uang_makan` int(11) DEFAULT '0',
  `uang_premi` int(11) DEFAULT '0',
  `gaji_lain` int(11) DEFAULT '0',
  `bon_barang` int(11) DEFAULT '0',
  `bon_uang` int(11) DEFAULT '0',
  `terlambat` int(11) DEFAULT '0',
  `potongan_lain` int(11) DEFAULT '0',
  `total` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payrolls`
--

LOCK TABLES `payrolls` WRITE;
/*!40000 ALTER TABLE `payrolls` DISABLE KEYS */;
/*!40000 ALTER TABLE `payrolls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelanggan`
--

DROP TABLE IF EXISTS `pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelanggan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `gaji_pokok` decimal(19,4) DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelanggan`
--

LOCK TABLES `pelanggan` WRITE;
/*!40000 ALTER TABLE `pelanggan` DISABLE KEYS */;
INSERT INTO `pelanggan` VALUES (1,'KRY1','DIAN',0.0000,NULL,NULL,NULL,NULL,NULL),(2,'KRY2','DEVINDA',0.0000,NULL,NULL,NULL,NULL,NULL),(3,'KRY3','TIKA',0.0000,NULL,NULL,NULL,NULL,NULL),(4,'KRY4','ESTER',0.0000,NULL,NULL,NULL,NULL,NULL),(5,'KRY5','SHOFI',0.0000,NULL,NULL,NULL,NULL,NULL),(6,'KRY6','CICI',0.0000,NULL,NULL,NULL,NULL,NULL),(7,'KRY7','YASIN',0.0000,NULL,NULL,NULL,NULL,NULL),(8,'KRY8','ALAIK',0.0000,NULL,NULL,NULL,NULL,NULL),(9,'KRY9','ARIP',0.0000,NULL,NULL,NULL,NULL,NULL),(10,'KRY10','KHOIRUL',0.0000,NULL,NULL,NULL,NULL,NULL),(11,'KRY11','BENI',0.0000,NULL,NULL,NULL,NULL,NULL),(12,'KRY12','NISA',0.0000,NULL,NULL,NULL,NULL,NULL),(13,'KRY13','ERNI',0.0000,NULL,NULL,NULL,NULL,NULL),(14,'KRY14','PONGKI',0.0000,NULL,NULL,NULL,NULL,NULL),(15,'KRY15','ANDRE',0.0000,NULL,NULL,NULL,NULL,NULL),(16,'KRY16','DANNES',0.0000,NULL,NULL,NULL,NULL,NULL),(17,'KRY17','ROBBY',0.0000,NULL,NULL,NULL,NULL,NULL),(18,'KRY18','EKO',0.0000,NULL,NULL,NULL,NULL,NULL),(19,'KRY19','BAYU',0.0000,NULL,NULL,NULL,NULL,NULL),(20,'KRY20','EKA',0.0000,NULL,NULL,NULL,NULL,NULL),(21,'KRY21','SITI',0.0000,NULL,NULL,NULL,NULL,NULL),(22,'KRY22','TOMI',0.0000,NULL,NULL,NULL,NULL,NULL),(23,'KRY23','ATAK',0.0000,NULL,NULL,NULL,NULL,NULL),(24,'KRY24','EDI GEOL',0.0000,NULL,NULL,NULL,NULL,NULL),(25,'KRY25','SUYITNO',0.0000,NULL,NULL,NULL,NULL,NULL),(26,'KRY26','UDIN',0.0000,NULL,NULL,NULL,NULL,NULL),(27,'KRY27','HERI',0.0000,NULL,NULL,NULL,NULL,NULL),(28,'KRY28','WIGNYO',0.0000,NULL,NULL,NULL,NULL,NULL),(29,'KRY29','SATRIA',0.0000,NULL,NULL,NULL,NULL,NULL),(30,'KRY30','HAFID',0.0000,NULL,NULL,NULL,NULL,NULL),(31,'KRY31','KOESNUL',0.0000,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengguna`
--

DROP TABLE IF EXISTS `pengguna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengguna` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('admin','pegawai') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengguna`
--

LOCK TABLES `pengguna` WRITE;
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;
INSERT INTO `pengguna` VALUES (1,'admin','$2y$10$BqZLf5qWemFOsSz5YGDXU.J6pAv99kNkcoBFjbWd4x6rjzyaFTf3C','admin','btxPUlFVgXRrWHS2v9EejPPrlBJnCl3BxlKfirSrhru1UzbMCM8DDQ4G2FzV',NULL,'2019-08-21 22:25:09','2019-08-24 00:50:52');
/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-25 19:37:07
